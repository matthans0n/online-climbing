<?php

 use Illuminate\Database\Schema\Blueprint;
 use Illuminate\Database\Migrations\Migration;

class CreateIndoorLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('indoor_locations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('url_slug');
			$table->string('website');
			$table->string('contact_email');
			$table->string('contact_number');
			$table->longText('description');
			$table->text('prices');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('indoor_locations');
	}

}
