<?php

return [

	'name' => 'Name',
	'name_help' => 'Enter the Name here',
	'url_slug' => 'Url Slug',
	'url_slug_help' => 'Enter the Url Slug here',
	'website' => 'Website',
	'website_help' => 'Enter the Website here',
	'contact_email' => 'Contact Email',
	'contact_email_help' => 'Enter the Contact Email here',
	'contact_number' => 'Contact Number',
	'contact_number_help' => 'Enter the Contact Number here',
	'description' => 'Description',
	'description_help' => 'Enter the Description here',
	'prices' => 'Prices',
	'prices_help' => 'Enter the Prices here',

];
