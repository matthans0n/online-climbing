<?php

return [

	'index'  => 'List Indoor locations',
	'create' => 'Create new Indoor location',
	'edit'   => 'Edit Indoor location',
	'delete' => 'Delete Indoor location',

];
