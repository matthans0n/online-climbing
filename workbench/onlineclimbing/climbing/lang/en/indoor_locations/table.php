<?php

return [

	'id' => 'Id',
	'name' => 'Name',
	'url_slug' => 'Url Slug',
	'website' => 'Website',
	'contact_email' => 'Contact Email',
	'contact_number' => 'Contact Number',
	'description' => 'Description',
	'prices' => 'Prices',
	'created_at' => 'Created At',

];
