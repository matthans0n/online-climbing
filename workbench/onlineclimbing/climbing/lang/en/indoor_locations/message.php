<?php

return [

	// General messages
	'not_found' => 'Indoor location [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Indoor location was successfully created.',
		'update' => 'Indoor location was successfully updated.',
		'delete' => 'Indoor location was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the indoor location. Please try again.',
		'update' => 'There was an issue updating the indoor location. Please try again.',
		'delete' => 'There was an issue deleting the indoor location. Please try again.',
	],

];
