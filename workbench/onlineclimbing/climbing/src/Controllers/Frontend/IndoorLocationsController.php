<?php namespace Onlineclimbing\Climbing\Controllers\Frontend;

use Onlineclimbing\Climbing\Repositories\IndoorLocationRepositoryInterface;
use Platform\Foundation\Controllers\BaseController;
use View;

/**
 * Class IndoorLocationsController
 * @package Onlineclimbing\Climbing\Controllers\Frontend
 */
class IndoorLocationsController extends BaseController {

    /**
     * @var \Onlineclimbing\Climbing\Repositories\IndoorLocationRepositoryInterface
     */
    private $indoorLocations;

    /**
     * @param IndoorLocationRepositoryInterface $indoorLocations
     */
    public function __construct(IndoorLocationRepositoryInterface $indoorLocations)
    {

        $this->indoorLocations = $indoorLocations;
    }

	/**
	 * Return the main view.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return View::make('onlineclimbing/climbing::index');
	}

    /**
     * @param $slug
     */
    public function single($slug)
    {
        return $this->indoorLocations->findBySlug($slug);
    }

}
