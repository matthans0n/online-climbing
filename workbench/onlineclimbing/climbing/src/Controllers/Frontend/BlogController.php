<?php namespace Onlineclimbing\Climbing\Controllers\Frontend;

use Platform\Foundation\Controllers\BaseController;
use View;

class BlogController extends BaseController
{

    public function __construct()
    {

    }

    /**
     * Return the main blog list view.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return View::make('onlineclimbing/climbing::index');
    }

    /**
     * Return a single blog post
     *
     * @param $slug
     *
     * @return string
     */
    public function single($slug)
    {
        return 'Single blog with slug of ' . $slug;
    }

}
