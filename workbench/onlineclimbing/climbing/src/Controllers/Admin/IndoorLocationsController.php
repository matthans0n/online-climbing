<?php namespace Onlineclimbing\Climbing\Controllers\Admin;

use DataGrid;
use Input;
use Lang;
use Platform\Admin\Controllers\Admin\AdminController;
use Redirect;
use Response;
use View;
use Onlineclimbing\Climbing\Repositories\IndoorLocationRepositoryInterface;

class IndoorLocationsController extends AdminController {

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The Climbing repository.
	 *
	 * @var \Onlineclimbing\Climbing\Repositories\IndoorLocationRepositoryInterface
	 */
	protected $indoorLocation;

	/**
	 * Holds all the mass actions we can execute.
	 *
	 * @var array
	 */
	protected $actions = [
		'delete',
		'enable',
		'disable',
	];

	/**
	 * Constructor.
	 *
	 * @param  \Onlineclimbing\Climbing\Repositories\IndoorLocationRepositoryInterface  $indoorLocation
	 * @return void
	 */
	public function __construct(IndoorLocationRepositoryInterface $indoorLocation)
	{
		parent::__construct();

		$this->indoorLocation = $indoorLocation;
	}

	/**
	 * Display a listing of indoorLocation.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return View::make('onlineclimbing/climbing::indoor_locations.index');
	}

	/**
	 * Datasource for the indoorLocation Data Grid.
	 *
	 * @return \Cartalyst\DataGrid\DataGrid
	 */
	public function grid()
	{
		$data = $this->indoorLocation->grid();

		$columns = [
			'id',
			'name',
			'url_slug',
			'website',
			'contact_email',
			'contact_number',
			'description',
			'prices',
			'created_at',
		];

		$settings = [
			'sort'      => 'created_at',
			'direction' => 'desc',
		];

		return DataGrid::make($data, $columns, $settings);
	}

	/**
	 * Show the form for creating new indoorLocation.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new indoorLocation.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating indoorLocation.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating indoorLocation.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified indoorLocation.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		if ($this->indoorLocation->delete($id))
		{
			$message = Lang::get('onlineclimbing/climbing::indoor_locations/message.success.delete');

			return Redirect::toAdmin('climbing/indoor_locations')->withSuccess($message);
		}

		$message = Lang::get('onlineclimbing/climbing::indoor_locations/message.error.delete');

		return Redirect::toAdmin('climbing/indoor_locations')->withErrors($message);
	}

	/**
	 * Executes the mass action.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function executeAction()
	{
		$action = Input::get('action');

		if (in_array($action, $this->actions))
		{
			foreach (Input::get('entries', []) as $entry)
			{
				$this->indoorLocation->{$action}($entry);
			}

			return Response::json('Success');
		}

		return Response::json('Failed', 500);
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{
		// Do we have a indoorLocation identifier?
		if (isset($id))
		{
			if ( ! $indoorLocation = $this->indoorLocation->find($id))
			{
				$message = Lang::get('onlineclimbing/climbing::indoor_locations/message.not_found', compact('id'));

				return Redirect::toAdmin('climbing/indoor_locations')->withErrors($message);
			}
		}
		else
		{
			$indoorLocation = $this->indoorLocation->createModel();
		}

		// Show the page
		return View::make('onlineclimbing/climbing::indoor_locations.form', compact('mode', 'indoorLocation'));
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Get the input data
		$data = Input::all();

		// Do we have a indoorLocation identifier?
		if ($id)
		{
			// Check if the data is valid
			$messages = $this->indoorLocation->validForUpdate($id, $data);

			// Do we have any errors?
			if ($messages->isEmpty())
			{
				// Update the indoorLocation
				$indoorLocation = $this->indoorLocation->update($id, $data);
			}
		}
		else
		{
			// Check if the data is valid
			$messages = $this->indoorLocation->validForCreation($data);

			// Do we have any errors?
			if ($messages->isEmpty())
			{
				// Create the indoorLocation
				$indoorLocation = $this->indoorLocation->create($data);
			}
		}

		// Do we have any errors?
		if ($messages->isEmpty())
		{
			// Prepare the success message
			$message = Lang::get("onlineclimbing/climbing::indoor_locations/message.success.{$mode}");

			return Redirect::toAdmin("climbing/indoor_locations/{$indoorLocation->id}/edit")->withSuccess($message);
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

}
