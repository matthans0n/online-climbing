<?php namespace Onlineclimbing\Climbing\Models;

use Platform\Attributes\Models\Entity;

class IndoorLocation extends Entity {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'indoor_locations';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $eavNamespace = 'onlineclimbing/climbing.indoor_location';

}
