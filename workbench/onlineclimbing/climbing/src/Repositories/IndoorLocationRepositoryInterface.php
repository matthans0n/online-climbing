<?php namespace Onlineclimbing\Climbing\Repositories;

interface IndoorLocationRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Onlineclimbing\Climbing\Models\IndoorLocation
	 */
	public function grid();

	/**
	 * Returns all the climbing entries.
	 *
	 * @return \Onlineclimbing\Climbing\Models\IndoorLocation
	 */
	public function findAll();

	/**
	 * Returns a climbing entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Onlineclimbing\Climbing\Models\IndoorLocation
	 */
	public function find($id);

	/**
	 * Determines if the given climbing is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given climbing is valid for update.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates a climbing entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Onlineclimbing\Climbing\Models\IndoorLocation
	 */
	public function create(array $data);

	/**
	 * Updates the climbing entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Onlineclimbing\Climbing\Models\IndoorLocation
	 */
	public function update($id, array $data);

	/**
	 * Deletes the climbing entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

    /**
     * Find the climbing entry using the slug
     *
     * @param $slug
     *
     * @return mixed
     */
    public function findBySlug($slug);
}
