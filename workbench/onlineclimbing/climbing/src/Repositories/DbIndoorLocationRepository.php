<?php namespace Onlineclimbing\Climbing\Repositories;

use Cartalyst\Interpret\Interpreter;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Onlineclimbing\Climbing\Models\IndoorLocation;
use Symfony\Component\Finder\Finder;
use Validator;

class DbIndoorLocationRepository implements IndoorLocationRepositoryInterface {

	/**
	 * The Eloquent climbing model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * The event dispatcher instance.
	 *
	 * @var \Illuminate\Events\Dispatcher
	 */
	protected $dispatcher;

	/**
	 * Holds the form validation rules.
	 *
	 * @var array
	 */
	protected $rules = [

	];

	/**
	 * Constructor.
	 *
	 * @param  string  $model
	 * @param  \Illuminate\Events\Dispatcher  $dispatcher
	 * @return void
	 */
	public function __construct($model, Dispatcher $dispatcher)
	{
		$this->model = $model;

		$this->dispatcher = $dispatcher;
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this
			->createModel()
			->newQuery()
			->get();
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this
			->createModel()
			->where('id', (int) $id)
			->first();
	}

    public function findBySlug($slug)
    {
        return $this->createModel()
            ->where('url_slug', $slug)
            ->first();
    }

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $data)
	{
		return $this->validateIndoorLocation($data);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $data)
	{
		return $this->validateIndoorLocation($data);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $data)
	{
		with($indoorlocation = $this->createModel())->fill($data)->save();

		$this->dispatcher->fire('onlineclimbing.climbing.indoorlocation.created', $indoorlocation);

		return $indoorlocation;
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $data)
	{
		$indoorlocation = $this->find($id);

		$indoorlocation->fill($data)->save();

		$this->dispatcher->fire('onlineclimbing.climbing.indoorlocation.updated', $indoorlocation);

		return $indoorlocation;
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		if ($indoorlocation = $this->find($id))
		{
			$this->dispatcher->fire('onlineclimbing.climbing.indoorlocation.deleted', $indoorlocation);

			$indoorlocation->delete();

			return true;
		}

		return false;
	}

	/**
	 * Create a new instance of the model.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Database\Eloquent\Model
	 */
	public function createModel(array $data = [])
	{
		$class = '\\'.ltrim($this->model, '\\');

		return new $class($data);
	}

	/**
	 * Validates a climbing entry.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateIndoorLocation($data)
	{
		$validator = Validator::make($data, $this->rules);

		$validator->passes();

		return $validator->errors();
	}

}
