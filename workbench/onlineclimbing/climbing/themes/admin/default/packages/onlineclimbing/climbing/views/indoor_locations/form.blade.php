@extends('layouts/default')

{{-- Page title --}}
@section('title')
	@parent
	: {{{ trans("onlineclimbing/climbing::indoor_locations/general.{$mode}") }}} {{{ $indoorLocation->exists ? '- ' . $indoorLocation->name : null }}}
@stop

{{-- Queue assets --}}
{{ Asset::queue('bootstrap.tabs', 'bootstrap/js/tab.js', 'jquery') }}
{{ Asset::queue('climbing', 'onlineclimbing/climbing::js/script.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('content')

{{-- Page header --}}
<div class="page-header">

	<h1>{{{ trans("onlineclimbing/climbing::indoor_locations/general.{$mode}") }}} <small>{{{ $indoorLocation->name }}}</small></h1>

</div>

{{-- Content form --}}
<form id="climbing-form" action="{{ Request::fullUrl() }}" method="post" accept-char="UTF-8" autocomplete="off">

	{{-- CSRF Token --}}
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	{{-- Tabs --}}
	<ul class="nav nav-tabs">
		<li class="active"><a href="#general" data-toggle="tab">{{{ trans('onlineclimbing/climbing::general.tabs.general') }}}</a></li>
		<li><a href="#attributes" data-toggle="tab">{{{ trans('onlineclimbing/climbing::general.tabs.attributes') }}}</a></li>
	</ul>

	{{-- Tabs content --}}
	<div class="tab-content tab-bordered">

		{{-- General tab --}}
		<div class="tab-pane active" id="general">

			<div class="row">

				<div class="form-group{{ $errors->first('name', ' has-error') }}">

					<label for="name" class="control-label">{{{ trans('onlineclimbing/climbing::indoor_locations/form.name') }}} <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('onlineclimbing/climbing::indoor_locations/form.name_help') }}}"></i></label>

					<input type="text" class="form-control" name="name" id="name" placeholder="{{{ trans('onlineclimbing/climbing::indoor_locations/form.name') }}}" value="{{{ Input::old('name', $indoorLocation->name) }}}">

					<span class="help-block">{{{ $errors->first('name', ':message') }}}</span>

				</div>

				<div class="form-group{{ $errors->first('url_slug', ' has-error') }}">

					<label for="url_slug" class="control-label">{{{ trans('onlineclimbing/climbing::indoor_locations/form.url_slug') }}} <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('onlineclimbing/climbing::indoor_locations/form.url_slug_help') }}}"></i></label>

					<input type="text" class="form-control" name="url_slug" id="url_slug" placeholder="{{{ trans('onlineclimbing/climbing::indoor_locations/form.url_slug') }}}" value="{{{ Input::old('url_slug', $indoorLocation->url_slug) }}}">

					<span class="help-block">{{{ $errors->first('url_slug', ':message') }}}</span>

				</div>

				<div class="form-group{{ $errors->first('website', ' has-error') }}">

					<label for="website" class="control-label">{{{ trans('onlineclimbing/climbing::indoor_locations/form.website') }}} <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('onlineclimbing/climbing::indoor_locations/form.website_help') }}}"></i></label>

					<input type="text" class="form-control" name="website" id="website" placeholder="{{{ trans('onlineclimbing/climbing::indoor_locations/form.website') }}}" value="{{{ Input::old('website', $indoorLocation->website) }}}">

					<span class="help-block">{{{ $errors->first('website', ':message') }}}</span>

				</div>

				<div class="form-group{{ $errors->first('contact_email', ' has-error') }}">

					<label for="contact_email" class="control-label">{{{ trans('onlineclimbing/climbing::indoor_locations/form.contact_email') }}} <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('onlineclimbing/climbing::indoor_locations/form.contact_email_help') }}}"></i></label>

					<input type="text" class="form-control" name="contact_email" id="contact_email" placeholder="{{{ trans('onlineclimbing/climbing::indoor_locations/form.contact_email') }}}" value="{{{ Input::old('contact_email', $indoorLocation->contact_email) }}}">

					<span class="help-block">{{{ $errors->first('contact_email', ':message') }}}</span>

				</div>

				<div class="form-group{{ $errors->first('contact_number', ' has-error') }}">

					<label for="contact_number" class="control-label">{{{ trans('onlineclimbing/climbing::indoor_locations/form.contact_number') }}} <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('onlineclimbing/climbing::indoor_locations/form.contact_number_help') }}}"></i></label>

					<input type="text" class="form-control" name="contact_number" id="contact_number" placeholder="{{{ trans('onlineclimbing/climbing::indoor_locations/form.contact_number') }}}" value="{{{ Input::old('contact_number', $indoorLocation->contact_number) }}}">

					<span class="help-block">{{{ $errors->first('contact_number', ':message') }}}</span>

				</div>

				<div class="form-group{{ $errors->first('description', ' has-error') }}">

					<label for="description" class="control-label">{{{ trans('onlineclimbing/climbing::indoor_locations/form.description') }}} <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('onlineclimbing/climbing::indoor_locations/form.description_help') }}}"></i></label>

					<textarea class="form-control" name="description" id="description" placeholder="{{{ trans('onlineclimbing/climbing::indoor_locations/form.description') }}}">{{{ Input::old('description', $indoorLocation->description) }}}</textarea>

					<span class="help-block">{{{ $errors->first('description', ':message') }}}</span>

				</div>

				<div class="form-group{{ $errors->first('prices', ' has-error') }}">

					<label for="prices" class="control-label">{{{ trans('onlineclimbing/climbing::indoor_locations/form.prices') }}} <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('onlineclimbing/climbing::indoor_locations/form.prices_help') }}}"></i></label>

					<textarea class="form-control" name="prices" id="prices" placeholder="{{{ trans('onlineclimbing/climbing::indoor_locations/form.prices') }}}">{{{ Input::old('prices', $indoorLocation->prices) }}}</textarea>

					<span class="help-block">{{{ $errors->first('prices', ':message') }}}</span>

				</div>


			</div>

		</div>

		{{-- Attributes tab --}}
		<div class="tab-pane clearfix" id="attributes">

			@widget('platform/attributes::entity.form', [$indoorLocation])

		</div>

	</div>

	{{-- Form actions --}}
	<div class="row">

		<div class="col-lg-12 text-right">

			{{-- Form actions --}}
			<div class="form-group">

				<button class="btn btn-success" type="submit">{{{ trans('button.save') }}}</button>

				<a class="btn btn-default" href="{{{ URL::toAdmin('climbing/indoor_locations') }}}">{{{ trans('button.cancel') }}}</a>

				<a class="btn btn-danger" data-toggle="modal" data-target="modal-confirm" href="{{ URL::toAdmin("climbing/indoor_locations/{$indoorLocation->id}/delete") }}">{{{ trans('button.delete') }}}</a>

			</div>

		</div>

	</div>

</form>

@stop
