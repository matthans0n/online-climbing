<script type="text/template" data-grid="indoor_location" data-template="results">

	<% _.each(results, function(r) { %>

		<tr>
			<td><input content="id" name="entries[]" type="checkbox" value="<%= r.id %>"></td>
			<td><a href="{{ URL::toAdmin('climbing/indoor_locations/<%= r.id %>/edit') }}"><%= r.id %></a></td>
			<td><%= r.name %></td>
			<td><%= r.url_slug %></td>
			<td><%= r.website %></td>
			<td><%= r.contact_email %></td>
			<td><%= r.contact_number %></td>
			<td><%= r.description %></td>
			<td><%= r.prices %></td>
			<td><%= r.created_at %></td>
		</tr>

	<% }); %>

</script>
