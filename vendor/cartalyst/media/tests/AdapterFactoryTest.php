<?php namespace Cartalyst\Media\Tests;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Media\Adapters\AdapterFactory;
use PHPUnit_Framework_TestCase;

class AdapterFactoryTest extends PHPUnit_Framework_TestCase {

	/**
	 * Holds the adapter factory instance.
	 *
	 * @var \Cartalyst\Media\Adapters\AdapterFactory
	 */
	protected $adapter;

	/**
	 * Setup resources and dependencies
	 */
	public function setUp()
	{
		$this->adapter = new AdapterFactory();
	}

	/** @test */
	public function it_can_be_instantiated()
	{
		$this->assertInstanceOf('Cartalyst\Media\Adapters\AdapterFactory', $this->adapter);
	}

	/**
	 * @test
	 * @expectedException \InvalidArgumentException
	 */
	public function it_throws_an_invalid_argument_exception_if_no_matching_adapter_is_found()
	{
		$adapter = [
			'adapter' => 'invalid',
			'path'    => 'public/media',
		];

		$this->adapter->make($adapter);
	}

	/**
	 * @test
	 * @expectedException \InvalidArgumentException
	 */
	public function it_throws_an_invalid_argument_exception_if_no_adapter_is_set()
	{
		$adapter = [
			'path'    => 'public/media',
		];

		$this->adapter->make($adapter);
	}

}
