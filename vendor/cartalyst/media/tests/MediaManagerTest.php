<?php namespace Cartalyst\Media\Tests;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Media\MediaManager;
use Mockery as m;
use PHPUnit_Framework_TestCase;

class MediaManagerTest extends PHPUnit_Framework_TestCase {

	/**
	 * Holds the media manager instance.
	 *
	 * @var \Cartalyst\Media\MediaManager
	 */
	protected $media;

	/**
	 * Holds the media config.
	 *
	 * @var array
	 */
	protected $config;

	/**
	 * Close mockery.
	 *
	 * @return void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * Setup resources and dependencies
	 */
	public function setUp()
	{
		$this->config = require __DIR__.'/../src/config/config.php';

		$this->media = new MediaManager($this->config);

		$this->media->setMaxFileSize(10); // 10 MB

		$this->media->setAllowedMimes([
			'image/jpeg',
		]);
	}

	/** @test */
	public function it_can_be_instantiated()
	{
		$media = new MediaManager([]);

		$this->assertInstanceOf('Cartalyst\Media\MediaManager', $media);
	}

	/** @test */
	public function it_uses_local_as_default_connection()
	{
		$this->assertEquals('local', $this->media->getDefaultConnection());
	}

	/** @test */
	public function it_can_switch_connections()
	{
		$this->media->setDefaultConnection('dropbox');

		$this->assertEquals('dropbox', $this->media->getDefaultConnection());
	}

	/** @test */
	public function it_can_set_and_retrieve_defaults()
	{
		$this->assertInstanceOf('Cartalyst\Media\Filesystem', $this->media->connection());


		$this->media->setMaxFileSize(20);
		$this->assertEquals(20, $this->media->getMaxFileSize());


		$mimes = [
			'audio/ogg',
			'image/jpeg',
		];
		$this->media->setAllowedMimes($mimes);
		$this->assertSame($mimes, $this->media->getAllowedMimes());


		$placeholders = [
			':yyyy' => date('Y'),
			':yy'   => date('y'),
		];
		$this->media->setPlaceholders($placeholders);
		$this->assertSame($placeholders, $this->media->getPlaceholders());


		$dispersion = ':yyyy/:mm';
		$this->media->setDispersion($dispersion);
		$this->assertSame($dispersion, $this->media->getDispersion());


		$connection = 'dropbox';
		$this->media->setDefaultConnection($connection);
		$this->assertSame($connection, $this->media->getDefaultConnection());

		// $this->media->__call($method, $parameters)
	}

	/**
	 * @test
	 * @expectedException \InvalidArgumentException
	 */
	public function it_throws_an_invalid_argument_exception_on_invalid_connections()
	{
		$this->assertInstanceOf('Cartalyst\Media\Filesystem', $this->media->connection('invalid'));
	}

	/** @test */
	public function it_dynamically_passes_methods_to_the_connection()
	{
		$this->media = new MediaManager($this->config, $connection = m::mock('Cartalyst\Media\ConnectionFactory'));

		$connection->shouldReceive('make')->once()->andReturn($filesystem = m::mock('Cartalyst\Media\Filesystem'));

		$filesystem->shouldReceive('write')->once();

		$this->media->write('test', 'content');
	}

}
