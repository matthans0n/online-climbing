<?php namespace Cartalyst\Media\Tests;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Media\File;
use Mockery as m;
use PHPUnit_Framework_TestCase;

class FileTest extends PHPUnit_Framework_TestCase {

	/**
	 * Holds the media manager instance.
	 *
	 * @var \Cartalyst\Media\File
	 */
	protected $file;

	/**
	 * Setup resources and dependencies
	 */
	public function setUp()
	{

	}

	/** @test */
	public function it_can_be_instantiated()
	{

	}

}
