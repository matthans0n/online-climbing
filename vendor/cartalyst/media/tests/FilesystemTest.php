<?php namespace Cartalyst\Media\Tests;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Media\Filesystem;
use Mockery as m;
use PHPUnit_Framework_TestCase;

class FilesystemTest extends PHPUnit_Framework_TestCase {

	/**
	 * Holds the media manager instance.
	 *
	 * @var \Cartalyst\Media\Filesystem
	 */
	protected $filesystem;

	/**
	 * Close mockery.
	 *
	 * @return void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * Setup resources and dependencies
	 */
	public function setUp()
	{
		$adapter = m::mock('League\Flysystem\AdapterInterface');

		$filesystem = new Filesystem($adapter);
	}

	/** @test */
	public function it_can_be_instantiated()
	{
		$adapter = m::mock('League\Flysystem\AdapterInterface');

		$filesystem = new Filesystem($adapter);

		$this->assertInstanceOf('Cartalyst\Media\Filesystem', $filesystem);
	}

	/** @test */
	public function it_can_validate_files()
	{
		$adapter = m::mock('League\Flysystem\AdapterInterface');
		$manager = m::mock('Cartalyst\Media\MediaManager');
		$file    = m::mock('Symfony\Component\HttpFoundation\File\UploadedFile');

		$filesystem = new Filesystem($adapter);

		$file->shouldReceive('getSize')->once();
		$file->shouldReceive('getMimeType')->once();

		$manager->shouldReceive('getMaxFileSize')->once();
		$manager->shouldReceive('getAllowedMimes')->once();

		$filesystem->setManager($manager);
		$filesystem->validateFile($file);
	}

	/**
	 * @test
	 * @expectedException \Cartalyst\Media\Exceptions\MaxFileSizeExceededException
	 */
	public function it_throws_an_exception_if_file_is_larger_than_max()
	{
		$adapter = m::mock('League\Flysystem\AdapterInterface');
		$manager = m::mock('Cartalyst\Media\MediaManager');
		$file    = m::mock('Symfony\Component\HttpFoundation\File\UploadedFile');

		$filesystem = new Filesystem($adapter);

		$file->shouldReceive('getSize')->once()->andReturn(20);
		$file->shouldReceive('getMimeType')->once();

		$manager->shouldReceive('getMaxFileSize')->once();
		$manager->shouldReceive('getAllowedMimes')->once();

		$filesystem->setManager($manager);
		$filesystem->validateFile($file);
	}

	/**
	 * @test
	 * @expectedException \Cartalyst\Media\Exceptions\InvalidMimeTypeException
	 */
	public function it_throws_an_exception_if_file_has_invalid_mime_type()
	{
		$adapter = m::mock('League\Flysystem\AdapterInterface');
		$manager = m::mock('Cartalyst\Media\MediaManager');
		$file    = m::mock('Symfony\Component\HttpFoundation\File\UploadedFile');

		$filesystem = new Filesystem($adapter);

		$file->shouldReceive('getSize')->once();
		$file->shouldReceive('getMimeType')->once()->andReturn('invalid');

		$manager->shouldReceive('getMaxFileSize')->once();
		$manager->shouldReceive('getAllowedMimes')->once()->andReturn(['image/jpeg']);

		$filesystem->setManager($manager);
		$filesystem->validateFile($file);
	}

	/**
	 * @test
	 */
	public function it_can_prepare_the_file_location_from_string()
	{
		$adapter = m::mock('League\Flysystem\AdapterInterface');
		$manager = m::mock('Cartalyst\Media\MediaManager');

		$filesystem = new Filesystem($adapter);

		$filesystem->setManager($manager);

		$manager->shouldReceive('getPlaceholders')->once()->andReturn([':yyyy' => '2014', ':mm' => '01']);
		$manager->shouldReceive('getDispersion')->once()->andReturn(':yyyy/:mm/');

		$this->assertEquals($filesystem->prepareFileLocation('file.png'), '2014/01/file.png');
	}

	/**
	 * @test
	 */
	public function it_can_prepare_the_file_location_from_uploaded_file()
	{
		$adapter = m::mock('League\Flysystem\AdapterInterface');
		$manager = m::mock('Cartalyst\Media\MediaManager');
		$file = m::mock('Symfony\Component\HttpFoundation\File\UploadedFile');

		$filesystem = new Filesystem($adapter);

		$filesystem->setManager($manager);

		$manager->shouldReceive('getPlaceholders')->once()->andReturn([':yyyy' => '2014', ':mm' => '01']);
		$manager->shouldReceive('getDispersion')->once()->andReturn(':extension/:yyyy/:mm/');

		$file->shouldReceive('getExtension')->once()->andReturn('jpg');
		$file->shouldReceive('getMimeType')->once()->andReturn('image/jpeg');

		$this->assertEquals($filesystem->prepareFileLocation($file, 'file.png'), 'jpg/2014/01/file.png');
	}

}
