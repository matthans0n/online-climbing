# Media

A framework agnostic filesystem package with multiple adapters like AwsS3 and Dropbox.

The package requires PHP 5.4+ and comes bundled with a Laravel 4 Facade and a Service Provider to simplify the optional framework integration and follows the FIG standard PSR-4 to ensure a high level of interoperability between shared PHP code and is fully unit-tested.

Part of the Cartalyst Arsenal & licensed [Cartalyst PSL](license.txt). Code well, rock on.

## Package Story

Package history and capabilities.

#### xx-May-14 - v1.0.0

- ```Media::has($file)``` Checks wether the file exists.
- ```Media::write($file, $contents)``` Write the content to a file.
- ```Media::update($file, $contents)```Update the file content.
- ```Media::put($file, $contents)``` Creates or updates the file.
- ```Media::read($file)``` Reads the file contents.
- ```Media::rename($file, $newFile)``` Renames the file.
- ```Media::copy($file, $newFilePath)``` Copies the file to another path.
- ```Media::delete($file)``` Deletes the file.
- ```Media::getSize($file)``` Returns the file size.
- ```Media::get($file)``` Returns a file object.
- ```Media::get($file)->read()``` Returns the file contents.
- ```Media::get($file)->getTimestamp()``` Returns the file timestamp.
- ```Media::get($file)->getMimetype()``` Returns the file mimetype.
- ```Media::get($file)->getSize()``` Returns the file size.
- ```Media::get($file)->getFullpath()``` Returns the file full path.
- ```Media::get($file)->isImage()``` Returns wether file is an image or not.
- ```Media::get($file)->getImageSize()``` Returns an array that contains the width and height if the file is an image.
- ```Media::get($file)->getExtension()``` Returns the file extension.
- ```Media::get($file)->delete()``` Deletes the file.
- ```Media::connection('dropbox')->has($file)``` Chose another connection.
- ```Media::upload($uploadedFile, $destination)``` Uploads a file.
- ```Media::getMaxFileSize()``` Returns the max file size limit.
- ```Media::setMaxFileSize(int $size)``` Sets the max file size limit.
- ```Media::getAllowedMimes()``` Returns all the allowed mime types for the upload.
- ```Media::setAllowedMimes(array $mimes)``` Sets the allowed mime types for the upload.
- ```Media::getPlaceholders()``` Returns all the placeholders.
- ```Media::setPlaceholders(array $placeholders)``` Sets the placeholders.
- ```Media::getDispersion()``` Returns the file dispersion path.
- ```Media::setDispersion($dispersion)``` Sets the file dispersion path.
- ```Media::getDefaultConnection()``` Returns the default connection name.
- ```Media::setDefaultConnection($name)``` Sets the default connection.

## Requirements

- PHP >=5.4

## Installation

Media is installable with Composer. Read further information on how to install.

[Installation Guide](http://cartalyst.com/manual/media#installation)

## Documentation

Refer to the following guide on how to use the Media package.

[Documentation](http://cartalyst.com/manual/media)

## Versioning

We version under the [Semantic Versioning](http://semver.org/) guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backward compatibility bumps the major (and resets the minor and patch)
* New additions without breaking backward compatibility bumps the minor (and resets the patch)
* Bug fixes and misc changes bumps the patch

## Contributing

Please read the [Contributing](contributing.md) guidelines.

## Support

Have a bug? Please create an [issue](https://github.com/cartalyst/media/issues) here on GitHub that conforms with [necolas's guidelines](https://github.com/necolas/issue-guidelines).

Follow us on Twitter, [@cartalyst](http://twitter.com/cartalyst).

Join us for a chat on IRC.

Server: irc.freenode.net
Channel: #cartalyst

Email: help@cartalyst.com
