<?php namespace Cartalyst\Media\Adapters;

trait ValidatorTrait {

	/**
	 * Validates required parameters.
	 *
	 * @param  array  $config
	 * @return void
	 * @throws \InvalidArgumentException
	 */
	protected function validate($config)
	{
		foreach ($this->required as $key)
		{
			if ( ! array_get($config, $key))
			{
				throw new \InvalidArgumentException("$key is required.");
			}
		}
	}

}
