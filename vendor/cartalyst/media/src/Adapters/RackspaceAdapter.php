<?php namespace Cartalyst\Media\Adapters;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use League\Flysystem\Adapter\Rackspace;
use OpenCloud\ObjectStore\Resource\Container;
use OpenCloud\OpenStack;

class RackspaceAdapter implements AdapterInterface {

	use ValidatorTrait;

	/**
	 * Required fields.
	 *
	 * @var array
	 */
	protected $required = [
		'username',
		'password',
		'endpoint',
		'container',
	];

	/**
	 * {@inheritDoc}
	 */
	public function connect(array $config)
	{
		$username = array_get($config, 'username');

		$password = array_get($config, 'password');

		$endpoint = array_get($config, 'endpoint');

		$container = array_get($config, 'container');

		$service = array_get($config, 'service');

		$region = array_get($config, 'region');

		$this->validate($config);

		$stack = new OpenStack($endpoint, compact('username', 'password'));

		$store = $stack->objectStoreService($service, $region);

		$client = $store->getContainer($container);

		return new Rackspace($client);
	}

}
