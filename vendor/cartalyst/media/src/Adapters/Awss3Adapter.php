<?php namespace Cartalyst\Media\Adapters;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Aws\S3\S3Client;
use League\Flysystem\Adapter\AwsS3;

class Awss3Adapter implements AdapterInterface {

	use ValidatorTrait;

	/**
	 * Required fields.
	 *
	 * @var array
	 */
	protected $required = [
		'key',
		'secret',
		'bucket',
	];

	/**
	 * {@inheritDoc}
	 */
	public function connect(array $config)
	{
		$key = array_get($config, 'key');

		$secret = array_get($config, 'secret');

		$bucket = array_get($config, 'bucket');

		$options = array_get($config, 'options', []);

		$prefix = array_get($config, 'prefix');

		$this->validate($config);

		$client = S3Client::factory($config);

		return new AwsS3($client, $bucket, $prefix, $options);
	}

}
