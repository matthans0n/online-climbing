<?php namespace Cartalyst\Media\Adapters;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use League\Flysystem\Adapter\Sftp;

class SftpAdapter implements AdapterInterface {

	use ValidatorTrait;

	/**
	 * Required fields.
	 *
	 * @var array
	 */
	protected $required = [
		'host',
		'username',
		'password',
	];

	/**
	 * {@inheritDoc}
	 */
	public function connect(array $config)
	{
		$this->validate($config);

		return new Sftp($config);
	}

}
