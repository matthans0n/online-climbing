<?php namespace Cartalyst\Media\Adapters;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

interface AdapterInterface {

	/**
	 * Creates a connection.
	 *
	 * @param  array  $config
	 * @return mixed
	 */
	public function connect(array $config);

}
