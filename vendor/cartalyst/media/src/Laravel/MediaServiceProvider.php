<?php namespace Cartalyst\Media\Laravel;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Media\Adapters\AdapterFactory;
use Cartalyst\Media\ConnectionFactory;
use Cartalyst\Media\MediaManager;
use Illuminate\Support\ServiceProvider;

class MediaServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		$this->package('cartalyst/media', 'cartalyst/media', __DIR__.'/..');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->registerMedia();
	}

	/**
	 * Register the Media class.
	 *
	 * @return void
	 */
	protected function registerMedia()
	{
		$this->app['media'] = $this->app->share(function($app)
		{
			$config = $app['config']->get('cartalyst/media::config');

			$manager = new MediaManager($config);

			$manager->setDispersion($config['dispersion']);

			$manager->setMaxFileSize($config['max_filesize']);

			$manager->setAllowedMimes($config['allowed_mimes']);

			$manager->setPlaceholders($config['placeholders']);

			return $manager;
		});
	}

}
