<?php namespace Cartalyst\Media;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use League\Flysystem\Filesystem as Flysystem;
use League\Flysystem\Handler;
use Symfony\Component\HttpFoundation\File\UploadedFile;


use Cartalyst\Media\Exceptions\FileExistsException;
use Cartalyst\Media\Exceptions\InvalidMimeTypeException;
use Cartalyst\Media\Exceptions\MaxFileSizeExceededException;
use League\Flysystem\FileExistsException as LeagueFileExistsException;


class Filesystem extends Flysystem {

	/**
	 * The Media manager.
	 *
	 * @var \Cartalyst\Media\MediaManager
	 */
	protected $manager;

	/**
	 * Sets the Media manager.
	 *
	 * @param  \Cartalyst\Media\MediaManager  $manager
	 * @return void
	 */
	public function setManager(MediaManager $manager)
	{
		$this->manager = $manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public function write($path, $contents, $config = null)
	{
		$path = $this->prepareFileLocation($path);

		parent::write($path, $contents, $config);

		return $this->get($path);
	}

	/**
	 * {@inheritDoc}
	 */
	public function put($path, $contents, $config = null)
	{
		$_path = $this->prepareFileLocation($path);

		$path = $this->has($_path) ? $_path : $path;

		return parent::put($path, $contents, $config);
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($path, $contents, $config = null)
	{
		parent::update($path, $contents, $config);

		return $this->get($path);
	}

	/**
	 * {@inheritDoc}
	 */
    public function get($path, Handler $handler = null)
	{
		return parent::get($path, new File($this, $path));
	}

	/**
	 * Check if the given file is valid.
	 *
	 * @param  \Symfony\Component\HttpFoundation\File\UploadedFile  $file
	 * @return bool
	 * @throws \Cartalyst\Media\Exceptions\MaxFileSizeExceededException
	 * @throws \Cartalyst\Media\Exceptions\InvalidMimeTypeException
	 */
	public function validateFile(UploadedFile $file)
	{
		$manager = $this->manager;

		$fileSize = $file->getSize();

		$fileMime = $file->getMimeType();

		$maxFileSize = $manager->getMaxFileSize();

		$allowedMimes = $manager->getAllowedMimes();

		// Validate the file size
		if ($fileSize > $maxFileSize)
		{
			throw new MaxFileSizeExceededException;
		}

		// Validate the file mime type
		if ( ! empty($allowedMimes) && ! in_array($fileMime, $allowedMimes))
		{
			throw new InvalidMimeTypeException;
		}

		return true;
	}

	/**
	 * Upload the file to the given destination.
	 *
	 * @param  \Symfony\Component\HttpFoundation\File\UploadedFile  $original
	 * @param  string  $destination
	 * @param  bool  $override
	 * @return \Cartalyst\Media\File
	 * @throws \Cartalyst\Media\Exceptions\FileExistsException
	 */
	public function upload(UploadedFile $original, $destination = null, $override = true)
	{
		try
		{
			if ( ! $destination)
			{
				$destination = $original->getClientOriginalName();

				$_destination = $this->prepareFileLocation($original, $destination);

				$method = ($this->has($_destination) && $override) ? 'update' : 'write';

				$destination = $method === 'update' ? $_destination : $destination;
			}
			else
			{
				$method = ($this->has($destination) && $override) ? 'update' : 'write';
			}

			$uploaded = $this->{$method}($destination, file_get_contents($original->getPathName()));

			return $this->get($uploaded->getPath());
		}
		catch (LeagueFileExistsException $e)
		{
			throw new FileExistsException;
		}
	}

	/**
	 * Prepares the file location name using the file dispersion feature.
	 *
	 * @param  \Symfony\Component\HttpFoundation\File\UploadedFile|string  $file
	 * @param  string  $destination
	 * @return string
	 */
	public function prepareFileLocation($file, $destination = null)
	{
		$manager = $this->manager;

		$placeholders = array_merge($manager->getPlaceholders(), [
			' ' => '_',
		]);

		if ($file instanceof UploadedFile)
		{
			$placeholders = array_merge($placeholders, [
				':extension' => $file->getExtension(),
				':mime'      => $file->getMimeType(),
			]);
		}

		$destination = ($file instanceof UploadedFile) ? $destination : $file;

		$destination = $manager->getDispersion().$destination;

		return str_replace(array_keys($placeholders), array_values($placeholders), $destination);
	}

}
