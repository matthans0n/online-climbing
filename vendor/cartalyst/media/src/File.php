<?php namespace Cartalyst\Media;
/**
 * Part of the Media package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Media
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use League\Flysystem\File as FileHandler;

class File extends FileHandler {

	/**
	 * Holds all the valid images MIME Types.
	 *
	 * @var array
	 */
	protected $imagesMimeTypes = [
		'image/bmp',
		'image/gif',
		'image/jpeg',
		'image/png',
	];

	/**
	 * Returns the file name without the extension.
	 *
	 * @return string
	 */
	public function getName()
	{
		return str_replace(".{$this->getExtension()}", '', $this->path);
	}

	/**
	 * Returns the file full path.
	 *
	 * @return string
	 */
	public function getFullpath()
	{
		return $this->filesystem->getAdapter()->applyPathPrefix($this->path);
	}

	/**
	 * Checks if the uploaded media is an image.
	 *
	 * @return bool
	 */
	public function isImage()
	{
		return in_array($this->getMimetype(), $this->imagesMimeTypes);
	}

	/**
	 * Return the image width and height.
	 *
	 * @return array
	 */
	public function getImageSize()
	{
		$raw = $this->filesystem->getAdapter()->read($this->path);

		$image = imagecreatefromstring($raw['contents']);

		$width  = imagesx($image);
		$height = imagesy($image);

		return compact('width', 'height');
	}

	/**
	 * Returns the file extension.
	 *
	 * @return string
	 */
	public function getExtension()
	{
		return pathinfo($this->path, PATHINFO_EXTENSION);
	}

}

