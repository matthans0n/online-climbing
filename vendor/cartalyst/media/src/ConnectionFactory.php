<?php namespace Cartalyst\Media;

use Cartalyst\Media\Adapters\AdapterFactory;
use Cartalyst\Media\MediaManager;

class ConnectionFactory {

	/**
	 * Adapter instance.
	 *
	 * @var \Cartalyst\Media\AdapterFactory
	 */
	protected $adapter;

	/**
	 * Constructor
	 *
	 * @param  \Cartalyst\Media\AdapterFactory  $adapter
	 * @return void
	 */
	public function __construct(AdapterFactory $adapter = null)
	{
		$this->adapter = $adapter ?: new AdapterFactory();
	}

	/**
	 * Create a new filesystem.
	 *
	 * @param  array  $config
	 * @param  \Cartalyst\Media\MediaManager $manager
	 * @return \Cartalyst\Media\Filesystem
	 */
	public function make($config, MediaManager $manager)
	{
		$adapter = $this->adapter->make($config);

		$filesystem = new Filesystem($adapter);

		$filesystem->setManager($manager);

		return $filesystem;
	}

}
