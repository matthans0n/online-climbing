<?php
/**
 * Part of the API package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    API
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

// Require this file from bootstrap/start.php by adding
// require_once __DIR__.'/../vendor/cartalyst/api/src/start.php';
// before $app = new Illuminate\Foundation\Application;
// Or just insert this line there.

/*
|--------------------------------------------------------------------------
| Override Request Class
|--------------------------------------------------------------------------
|
| By overriding our request class to that of the API package, we are able
| to inspect input as runtime objects rather than serialized strings.
|
*/

Illuminate\Foundation\Application::requestClass('Cartalyst\Api\Request');
