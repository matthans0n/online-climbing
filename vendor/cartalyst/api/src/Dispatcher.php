<?php namespace Cartalyst\Api;
/**
 * Part of the API package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    API
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Container\Container;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Facade;

class Dispatcher {

	/**
	 * The IoC container instance.
	 *
	 * @var \Illuminate\Container\Container
	 */
	protected $container;

	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	public function get($uri)
	{
		return $this->handle('GET', $uri);
	}

	public function delete($uri, $body = null)
	{
		return $this->handle('DELETE', $uri, $body);
	}

	public function put($uri, $body = null)
	{
		return $this->handle('PUT', $uri, $body);
	}

	public function patch($uri, $body = null)
	{
		return $this->handle('PATCH', $uri, $body);
	}

	public function post($uri, $body = null)
	{
		return $this->handle('POST', $uri, $body);
	}

	/**
	 * Handle API calls for named routes, replacing URL parameters.
	 *
	 * @param  string  $name
	 * @param  array   $parameters
	 * @param  string  $body
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function route($name, $parameters = array(), $body = null)
	{
		$route = $this->container['router']->getRoutes()->getByName($name);

		$uri = $this->container['url']->route($name, $parameters, false, $route);

		$method = $this->resolveMethod($route);

		return $this->handle($method, $uri, $body);
	}

	public function handle($method, $uri, $body = null)
	{
		$parent = $this->container['request'];
		$request = $this->createRequest($method, $uri, $body);

		$this->container->instance('request', $request);
		Facade::clearResolvedInstance('request');

		$response = $this->container['router']->handleSub($request);

		$this->container->instance('request', $parent);
		Facade::clearResolvedInstance('request');

		return $response;
	}

	/**
	 * Returns if the current request is a sub request or not.
	 *
	 * @return bool
	 */
	public function isSubRequest()
	{
		return $this->container['router']->isSubRequest();
	}

	protected function resolveMethod(Route $route)
	{
		$methods = $route->getMethods();

		// The only acceptable multiple methods for a route are both
		// GET and HEAD. A route should never have multiple
		// conflicting methods (e.g. GET and POST).
		if ($methods === array('GET', 'HEAD'))
		{
			return 'GET';
		}

		if (count($methods) > 0)
		{
			throw new \RuntimeException('Cannot resolve multiple methods ['.implode(', ', $methods).'] for ['.$route->getName().'] route.');
		}

		return reset($methods);
	}

	protected function createRequest($method, $uri, $body = null)
	{
		$parts = parse_url($uri);
		$uri   = $parts['path'];
		$query = array();

		if (isset($parts['query']))
		{
			parse_str($parts['query'], $query);
		}

		$current = $this->container['router']->getCurrentRequest();

		$cookies = $current->cookies->all();
		$files   = $current->files->all();
		$server  = $current->server->all();

		return Request::create($uri, $method, $query, $cookies, $files, $server, $body);
	}

}
