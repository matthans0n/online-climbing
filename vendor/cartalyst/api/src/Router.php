<?php namespace Cartalyst\Api;
/**
 * Part of the API package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    API
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Routing\Router as BaseRouter;
use Illuminate\Routing\RouteCollection;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class Router extends BaseRouter {

	protected $currentRequestType;

	/**
	 * {@inheritDoc}
	 */
	public function handle(SymfonyRequest $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
	{
		$this->currentRequestType = $type;

		$response = parent::handle($request, $type, $catch);

		$this->currentRequestType = null;

		return $response;
	}

	/**
	 * Handles a sub request.
	 *
	 * @param  \Symfony\Component\HttpFoundation\Request  $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function handleSub(SymfonyRequest $request)
	{
		$this->disableFilters();

		$response = $this->handle($request, HttpKernelInterface::SUB_REQUEST);

		$this->enableFilters();

		return $response;
	}

	/**
	 * {@inheritDoc}
	 */
	protected function prepareResponse($request, $response)
	{
		if ($this->isSubRequest())
		{
			return $response;
		}

		if ($response instanceof Response and ! $response->getContent())
		{
			$response->transform();
		}

		return parent::prepareResponse($request, $response);
	}

	/**
	 * Returns if the current request is a sub request or not.
	 *
	 * @return bool
	 */
	public function isSubRequest()
	{
		return $this->currentRequestType === HttpKernelInterface::SUB_REQUEST;
	}

	/**
	 * Set the routes on the router.
	 *
	 * @param  \Illuminate\Routing\RouteCollection  $routes
	 * @return void
	 */
	public function setRoutes(RouteCollection $routes)
	{
		$this->routes = $routes;
	}

}
