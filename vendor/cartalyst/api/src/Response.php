<?php namespace Cartalyst\Api;
/**
 * Part of the API package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    API
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Http\JsonResponse;

class Response extends JsonResponse {

	/**
	 * {@inheritdoc}
	 */
	public function getData($assoc = false, $depth = 512, $options = 0)
	{
		if (is_string($this->data))
		{
			return parent::getData($assoc, $depth, $options);
		}

		return $this->data;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setData($data = array())
	{
		$this->data = $data;
	}

	/**
	 * Transforms live data for a sub request into JSON encoded data.
	 *
	 * @return void
	 */
	public function transform()
	{
		parent::setData($this->data);
	}

}

