<?php namespace Cartalyst\Api;
/**
 * Part of the API package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    API
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Http\Request as BaseRequest;
use Symfony\Component\HttpFoundation\ParameterBag;

class Request extends BaseRequest {

	/**
	 * {@inheritDoc}
	 */
	public function json($key = null, $default = null)
	{
		if ( ! isset($this->json))
		{
			$json = $this->getContent();

			if ( ! is_array($json))
			{
				$json = (array) json_decode($json, true);
			}

			$this->json = new ParameterBag($json);
		}

		return parent::json($key, $default);
	}

}
