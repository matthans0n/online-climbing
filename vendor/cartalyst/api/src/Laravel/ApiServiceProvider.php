<?php namespace Cartalyst\Api\Laravel;
/**
 * Part of the API package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    API
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Api\Dispatcher;
use Cartalyst\Api\Router;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	protected $defer = false;

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		$this->package('cartalyst/api', 'cartalyst/api', __DIR__.'/..');

		$this->overrideRouter();
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->app['api.router'] = $this->app->share(function($app)
		{
			$router = new Router($app['events'], $app);

			if ($app['env'] == 'testing')
			{
				$router->disableFilters();
			}

			return $router;
		});

		$this->app['api'] = $this->app->share(function($app)
		{
			return new Dispatcher($app);
		});
	}

	/**
	 * Override the Laravel router.
	 *
	 * @return void
	 */
	protected function overrideRouter()
	{
		$originalRouter = $this->app['router'];

		$this->app['router'] = $this->app->share(function($app) use ($originalRouter)
		{
			$app['api.router']->setRoutes($originalRouter->getRoutes());

			return $app['api.router'];
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function provides()
	{
		return ['router'];
	}

}
