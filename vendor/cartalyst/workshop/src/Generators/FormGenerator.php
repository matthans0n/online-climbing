<?php namespace Cartalyst\Workshop\Generators;
/**
 * Part of the Workshop package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Workshop
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Support\Str;

class FormGenerator extends Generator {

	/**
	 * Create a new form.
	 *
	 * @param  array  $columns
	 * @param  bool  $interface
	 * @return void
	 */
	public function create($model, $columns = [], $view = 'form')
	{
		$this->writeLangFiles($columns, $model);

		$stub = $this->getStub('form.blade.stub');

		$el = [];

		foreach ($columns as $col)
		{
			switch ($col['type']) {
				case 'text':
				case 'mediumText':
				case 'longText':
					$inputStub = $this->getStub('form-textarea.stub');
					break;

				case 'boolean':
				case 'tinyInteger':
					$inputStub = $this->getStub('form-checkbox.stub');
					break;

				case 'string':
				default:
					$inputStub = $this->getStub('form-input.stub');
					break;
			}

			$el[] = $this->prepare($inputStub, [
				'field_name'         => $col['field'],
				'camel_model'        => camel_case(strtolower($model)),
				'plural_lower_model' => strtolower(Str::plural($model)),
			]);
		}

		$content = $this->prepare($stub, [
			'columns'            => implode("\n\t\t\t\t", $el),
			'camel_model'        => camel_case(strtolower($model)),
			'plural_lower_model' => strtolower(Str::plural($model)),
		]);

		$filePath = $this->path.'/themes/admin/default/packages/'.$this->extension->lowerVendor.'/'.$this->extension->lowerName.'/views/'.Str::plural(strtolower($model)).'/';

		$this->ensureDirectory($filePath);

		$this->files->put($filePath.$view.'.blade.php', $content);
	}

	/**
	 * {@inheritDoc}
	 */
	public function prepare($path, $args = [])
	{
		$content = parent::prepare($path, $args);

		foreach ((array) $this->extension as $key => $value)
		{
			$content = str_replace('{{'.snake_case($key).'}}', $value, $content);
		}

		return $content;
	}

	/**
	 * Writes the form language file.
	 *
	 * @param  array  $columns
	 * @return void
	 */
	protected function writeLangFiles($columns, $model)
	{
		$stub = $this->getStub('lang/en/form.stub');

		$values = [];

		foreach ($columns as $column)
		{
			$values[$column['field']] = Str::title($column['field']);
			$values[$column['field'].'_help'] = 'Enter the '.Str::title($column['field']).' here';
		}

		$filePath = $this->path.'/lang/en/'.strtolower(Str::plural($model)).'/';

		$this->ensureDirectory($filePath);

		$filePath .= 'form.php';

		if ($this->files->exists($filePath))
		{
			$trans = $this->files->getRequire($filePath);

			$values = array_merge($trans, $values);
		}

		$trans = $this->wrapArray($values, '');

		$content = $this->prepare($stub, [
			'fields' => trim($trans),
		]);

		$this->files->put($filePath, $content);
	}

}
