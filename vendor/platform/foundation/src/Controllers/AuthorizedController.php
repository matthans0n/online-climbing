<?php namespace Platform\Foundation\Controllers;
/**
 * Part of the Platform Foundation extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Foundation extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Routing\Controller;

class AuthorizedController extends BaseController {

	/**
	 * Array of whitelisted methods which do not need to be authorized.
	 *
	 * @var array
	 */
	protected $authWhitelist = [];

	/**
	 * Array of whitelisted methods which do not get
	 * compared to permissions for the user.
	 *
	 * @var array
	 */
	protected $permissionsWhitelist = [];

	/**
	 * Creates a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->beforeFilter('auth', ['except' => $this->authWhitelist]);

		$this->beforeFilter('permissions', ['except' => $this->permissionsWhitelist]);
	}

}
