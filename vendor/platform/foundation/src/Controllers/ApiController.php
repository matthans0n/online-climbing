<?php namespace Platform\Foundation\Controllers;
/**
 * Part of the Platform Foundation extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Foundation extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Api\Response;
use Controller;
use Input;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ApiController extends Controller {

	/**
	 * Array of whitelisted methods which do not need to be authorized.
	 *
	 * @var array
	 */
	protected $authWhitelist = [];

	/**
	 * Holds the default status code.
	 *
	 * @var int
	 */
	protected $statusCode = 200;

	/**
	 * The Fractal instance manager.
	 *
	 * @var \League\Fractal\Manager
	 */
	protected $fractal;

	/**
	 * Constructor.
	 *
	 * @param  \League\Fractal\Manager  $fractal
	 * @return void
	 */
	public function __construct(Manager $fractal)
	{
		$this->beforeFilter('api.auth', ['except' => $this->authWhitelist]);

		$this->fractal = $fractal;

		// Are we going to try and include embedded data?
		$this->fractal->parseIncludes(Input::get('include', []));
	}

	/**
	 * Getter for the status code.
	 *
	 * @return mixed
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}

	/**
	 * Setter for the status code.
	 *
	 * @param  int  $statusCode
	 * @return \Platform\Foundation\Controllers\ApiController
	 */
	public function setStatusCode($statusCode)
	{
		$this->statusCode = $statusCode;

		return $this;
	}

	/**
	 * Returns the given item.
	 *
	 * @param  mixed  $item
	 * @param  mixed  $callback
	 * @return \Cartalyst\Api\Response
	 */
	protected function respondWithItem($item, $callback)
	{
		$resource = new Item($item, $callback);

		$data = $this->fractal->createData($resource);

		return $this->respondWithArray($data->toArray());
	}

	/**
	 * Returns the given collection.
	 *
	 * @param  mixed  $collection
	 * @param  mixed  $callback
	 * @return \Cartalyst\Api\Response
	 */
	protected function respondWithCollection($collection, $callback)
	{
		$resource = new Collection($collection, $callback);

		$data = $this->fractal->createData($resource);

		return $this->respondWithArray($data->toArray());
	}

	/**
	 * Returns the given data.
	 *
	 * @param  array  $data
	 * @param  array  $headers
	 * @return \Cartalyst\Api\Response
	 */
	protected function respondWithArray(array $data, array $headers = [])
	{
		return new Response($data, $this->statusCode, $headers);
	}

	/**
	 * Returns a response with an error response code.
	 *
	 * @param  array  $item
	 * @param  int  $code
	 * @return \Cartalyst\Api\Response
	 */
	protected function responseWithErrors($errors, $code)
	{
		$errors = (array) $errors;

		return new Response(compact('errors'), $code);
	}

	/**
	 * Returns a response with the no content response code.
	 *
	 * @return \Cartalyst\Api\Response
	 */
	protected function responseWithNoContent()
	{
		return new Response('', 204);
	}

}
