<?php
/**
 * Part of the Platform Foundation extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Foundation extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Api\Response;

if ( ! Event::hasListeners('router.filter: api.auth'))
{
	Route::filter('api.auth', function($route, $request)
	{
		$username = Request::header('username');

		$password = Request::header('password');

		if ( ! Sentinel::stateless(compact('username', 'password')))
		{
			return new Response('Authorization required!', 401);
		}
	});
}

if ( ! Event::hasListeners('router.filter: permissions'))
{
	Route::filter('permissions', function($route, $request)
	{
		if (Sentinel::hasAccess('admin'))
		{
			return;
		}

		$action = $route->getActionName();

		if (Sentinel::hasAccess($action))
		{
			return;
		}

		$permissions = [];

		foreach (Extensions::allEnabled() as $extension)
		{
			$_permissions = value($extension->permissions);

			if ($_permissions)
			{
				$permissions = array_merge($permissions, $_permissions);
			}
		}

		if ($action = array_get($permissions, $action, null))
		{
			$message = Lang::get('platform/foundation::permissions.no_access_to', compact('action'));
		}
		else
		{
			$message = Lang::get('platform/foundation::permissions.no_access');
		}

		return Redirect::to('/')->withErrors($message);
	});
}
