<?php
/**
 * Part of the Platform Foundation extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Foundation extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return [

	/*
	|-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	| Installed Version
	|-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	|
	| This variable holds the current installed version of Platform.
	|
	| You are highly discouraged from touching this, ever.
	|
	| How It Works:
	|
	|  - On a blank installation, the installed version will be FALSE, which
	|    means Platform isn't installed.
	|
	|  - We'll check this version against the PLATFORM_VERSION constant and
	|    if this version is less, it means you have upgraded the Platform
	|    codebase, we'll then lock out the application and send you to
	|    the installer where you'll be taken through the upgrade process.
	|
	|
	| These are the main reasons why touching this variable is a BAD idea!
	|
	|
	|
	|			                                            7
	|			                       B                   MY
	|			                     q@                   @q
	|			                    @M          @        BG        J
	|			                   @B          @:       @@        Y7
	|			                  PB         ;@i        @J        @
	|			                  Br       :B@         :Bi       ML
	|			                  @,      @B@.  .      ;@r       B
	|			                  B@   rB@B5P@M.B      LBq   .rqB@.
	|			           ,      @B@B@Bv      i@B.    XG0@B@B@Fi7GF.
	|			          @      .B@u            @ 7  :@O@B.        ,YUr
	|			         uM     :B@r            .@   q@BF               v
	|			         LB  :i B@M             v@  B@B                   .
	|			          Z@ kM.@B              G8qB@L
	|			        5   B@BkZM             iB@B0
	|			  u;u   @       N0            MB@B
	|			  LOB    Mu    7B@N,    iMB@B@@G,
	|			    :BMi  GB@j:B@J :MB@B@Z7
	|			      :UBBL8Uijiiq BP5
	|			         :FO  7i j. @.
	|			            B@v7B@.  @
	|			            P@8S5Bvv Ev
	|			          BSB7   LB@7.@Mi
	|			       :@O7B@:   i@Bv v@ Ok      .S0@S
	|			     jBqriS@MkPGO@@k   @  i@B@B@B@BP,
	|			    @Y 7;:Z::;PB@@@ S iB: @880EP0Z@
	|			     M.PB.7@MOJ2:: .. E@,EMZXqPPPMB.
	|			           @@P7vi2L0i@@U.@NkkSPS8q
	|			           ZBB8OG0ZZ8ZM 78XPSP5XZF
	|			            ,BMPq5kSXP.:8XXXXkPSO2
	|			              O8qSFkXY.MqPkPkqqqPBB@Fi
	|			              @BESPSXqEqNP0q0qEP8GBB@B
	|			               :@ZNqqEPN0ZGOOMOBB@B7
	|			                @BOZGEZE8B@B@@@BP
	|			                 @OEkqXq0P
	|			                  MBMOBGE8
	|			                    vvrMB@B@,
	|
	*/

	'installed_version' => false,

];
