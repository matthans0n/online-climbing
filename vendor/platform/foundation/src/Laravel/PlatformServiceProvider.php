<?php namespace Platform\Foundation\Laravel;
/**
 * Part of the Platform Foundation extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Foundation extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Support\ServiceProvider;
use Platform\Foundation\Commands\ThemeCompileCommand;
use Platform\Foundation\Commands\UpgradeCommand;
use Platform\Foundation\Platform;

class PlatformServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		$this->package('platform/foundation', 'platform/foundation', __DIR__.'/..');

		require_once __DIR__.'/../filters.php';
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->registerPlatform();

		$this->registerThemeCompileCommand();

		$this->registerUpgradeCommand();
	}

	/**
	 * Register platform.
	 *
	 * @return void
	 */
	protected function registerPlatform()
	{
		$this->app['platform'] = $this->app->share(function($app)
		{
			return new Platform($app, $app['extensions']);
		});
	}

	/**
	 * Register the theme compile command.
	 *
	 * @return void
	 */
	protected function registerThemeCompileCommand()
	{
		$this->app['command.theme.compile'] = $this->app->share(function($app)
		{
			return new ThemeCompileCommand();
		});

		$this->commands('command.theme.compile');
	}

	/**
	 * Register the upgrade command.
	 *
	 * @return void
	 */
	protected function registerUpgradeCommand()
	{
		$this->app['command.platform.upgrade'] = $this->app->share(function($app)
		{
			return new UpgradeCommand($app['platform']);
		});

		$this->commands('command.platform.upgrade');
	}

}
