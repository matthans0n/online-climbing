<?php
/**
 * Part of the Platform Foundation extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Foundation extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Config\Repository as ConfigRepository;
use Mockery as m;
use Platform\Foundation\Platform;

class PlatformTest extends PHPUnit_Framework_TestCase {

	/**
	 * Close mockery.
	 *
	 * @return void
	 */
	public function tearDown()
	{
		m::close();
	}

	/** @test */
	public function it_can_recognize_platform_is_not_installed()
	{
		$app      = new Illuminate\Container\Container;
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = new Platform($app, $bag);

		$app['config'] = new ConfigRepository(m::mock('Illuminate\Config\LoaderInterface'), 'production');
		$app['config']
			->getLoader()
			->shouldReceive('load')
			->once()
			->with('production', 'installed_version', 'platform/foundation')
			->andReturn(null);

		$this->assertNull($platform->installedVersion());
	}

	/** @test */
	public function it_can_show_the_correct_installed_version()
	{
		$app      = new Illuminate\Container\Container;
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = new Platform($app, $bag);

		$app['config'] = new ConfigRepository(m::mock('Illuminate\Config\LoaderInterface'), 'production');
		$app['config']
			->getLoader()
			->shouldReceive('load')
			->once()
			->with('production', 'installed_version', 'platform/foundation')
			->andReturn('2.0.0');

		$this->assertEquals('2.0.0', $platform->installedVersion());
	}

	/** @test */
	public function it_can_check_if_the_version_is_valid()
	{
		$app      = new Illuminate\Container\Container;
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = new Platform($app, $bag);

		$this->assertRegExp('/^(\d{1,2})(\.)?(\d{1,2})?(\.)?(\d{1,2})?$/', $platform->codebaseVersion());
	}

	/** @_test */
	public function it_can_check_if_an_upgrade_is_required()
	{
		$app      = new Illuminate\Container\Container;
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = m::mock('Platform\Foundation\Platform[installedVersion,codebaseVersion]', $app, $bag);

		$platform->shouldReceive('installedVersion')->once()->andReturn('2.0.1');
		$platform->shouldReceive('codebaseVersion')->once()->andReturn('2.2.0');

		$this->assertTrue($platform->needsUpgrade());
	}

	/** @_test */
	public function it_can_compare_versions_correctly()
	{
		$app      = new Illuminate\Container\Container;
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = m::mock('Platform\Foundation\Platform[installedVersion,codebaseVersion]', $app, $bag);

		$platform->shouldReceive('installedVersion')->once()->andReturn('2.2.0');
		$platform->shouldReceive('codebaseVersion')->once()->andReturn('2.2');

		$this->assertFalse($platform->needsUpgrade());
	}

	/** @_test */
	public function it_can_compare_versions_correctly_when_the_codebase_is_lower_than_the_installed_version()
	{
		$app      = new Illuminate\Container\Container;
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = m::mock('Platform\Foundation\Platform[installedVersion,codebaseVersion]', $app, $bag);

		$platform->shouldReceive('installedVersion')->once()->andReturn('2.2.0');
		$platform->shouldReceive('codebaseVersion')->once()->andReturn('2.1.5');

		$this->assertFalse($platform->needsUpgrade());
	}

	/** @test */
	public function it_can_fire_event_before_booting()
	{
		$app      = new Illuminate\Container\Container;
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = new Platform($app, $bag);

		$app['events'] = new Illuminate\Events\Dispatcher;

		$app['events']->listen('platform.booting', function()
		{
			$_SERVER['__platform.booting'] = true;
		});

		$platform->beforeBoot();

		$this->assertTrue($_SERVER['__platform.booting']);
		unset($_SERVER['__platform.booting']);
	}

	/** @test */
	public function it_can_fire_event_after_booting()
	{
		$app      = new Illuminate\Container\Container;
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = new Platform($app, $bag);

		$app['events'] = new Illuminate\Events\Dispatcher;

		$app['events']->listen('platform.booted', function()
		{
			$_SERVER['__platform.booted'] = true;
		});

		$platform->afterBoot();

		$this->assertTrue($_SERVER['__platform.booted']);
		unset($_SERVER['__platform.booted']);
	}

	/** @_test */
	public function it_can_check_eligibility_when_running_in_console()
	{
		$app      = m::mock('Illuminate\Container\Container[runningInConsole]');
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = new Platform($app, $bag);

		$app->shouldReceive('runningInConsole')->once()->andReturn(true);

		$this->assertTrue($platform->checkRunningEligibility());
	}

	/** @_test */
	public function it_can_check_eligibility_when_not_installed_and_not_local()
	{
		$app      = m::mock('Illuminate\Container\Container[runningInConsole]');
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = m::mock('Platform\Foundation\Platform[isInstalled]');
		$platform->__construct($app, $bag);

		$app->shouldReceive('runningInConsole')->once()->andReturn(false);
		$platform->shouldReceive('isInstalled')->once()->andReturn(false);

		$app['request'] = $request = m::mock('Illuminate\Http\Request');
		$request->shouldReceive('path')->once()->andReturn('');
		$request->shouldReceive('getClientIp')->once()->andReturn('10.0.0.1');

		try
		{
			$platform->checkRunningEligibility();
		}
		catch (Symfony\Component\HttpKernel\Exception\HttpException $e)
		{
			$expecetedStatusCode = 403;

			if (($statusCode = $e->getStatusCode()) == $expecetedStatusCode)
			{
				return;
			}

			$this->fail("HTTP Exception was thrown however status code [$statusCode] was thrown, [$expecetedStatusCode] expected.");
		}

		$this->fail('HTTP Exception was not thrown as expected.');
	}

	/** @_test */
	public function it_can_check_eligibility_when_not_installed_and_local()
	{
		$app      = m::mock('Illuminate\Container\Container[runningInConsole]');
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = m::mock('Platform\Foundation\Platform[isInstalled]');
		$platform->__construct($app, $bag);

		$app->shouldReceive('runningInConsole')->once()->andReturn(false);
		$platform->shouldReceive('isInstalled')->once()->andReturn(false);

		$app['request'] = $request = m::mock('Illuminate\Http\Request');
		$request->shouldReceive('path')->once()->andReturn('');
		$request->shouldReceive('getClientIp')->once()->andReturn('127.0.0.1');

		$this->assertFalse($platform->checkRunningEligibility());
	}

	/** @_test */
	public function it_can_check_eligibility_but_database_fails()
	{
		$app      = m::mock('Illuminate\Container\Container[runningInConsole]');
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = m::mock('Platform\Foundation\Platform[isInstalled]');
		$platform->__construct($app, $bag);

		$app->shouldReceive('runningInConsole')->once()->andReturn(false);
		$platform->shouldReceive('isInstalled')->once()->andReturn(true);

		$app['db'] = $db = m::mock('Illuminate\Database\ConnectionResolverInterface');
		$db->shouldReceive('connection')->once()->andThrow(new PDOException);

		try
		{
			$platform->checkRunningEligibility();
		}
		catch (Symfony\Component\HttpKernel\Exception\HttpException $e)
		{
			$expecetedStatusCode = 503;

			if (($statusCode = $e->getStatusCode()) == $expecetedStatusCode)
			{
				return;
			}

			$this->fail("HTTP Exception was thrown however status code [$statusCode] was thrown, [$expecetedStatusCode] expected.");
		}

		$this->fail('HTTP Exception was not thrown as expected.');
	}

	/** @_test */
	public function it_can_check_eligibility_installed_and_database_passes()
	{
		$app      = m::mock('Illuminate\Container\Container[runningInConsole]');
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = m::mock('Platform\Foundation\Platform[isInstalled]');
		$platform->__construct($app, $bag);

		$app->shouldReceive('runningInConsole')->once()->andReturn(false);
		$platform->shouldReceive('isInstalled')->once()->andReturn(true);

		$app['db'] = $db = m::mock('Illuminate\Database\ConnectionResolverInterface');
		$db->shouldReceive('connection')->once();

		$this->assertTrue($platform->checkRunningEligibility());
	}

	/** @_test */
	public function it_can_listen_to_ineligible_to_run_event_is_fired()
	{
		$app      = new Illuminate\Container\Container;
		$bag      = m::mock('Cartalyst\Extensions\ExtensionBag');
		$platform = m::mock('Platform\Foundation\Platform[checkRunningEligibility]');
		$platform->__construct($app, $bag);

		$platform->shouldReceive('checkRunningEligibility')->once()->andReturn(false);

		$app['events'] = new Illuminate\Events\Dispatcher;

		$app['events']->listen('platform.ineligible', function()
		{
			$_SERVER['__platform.ineligible'] = true;
		});

		$platform->boot();

		$this->assertTrue($_SERVER['__platform.ineligible']);
		unset($_SERVER['__platform.ineligible']);
	}

	/** @test */
	public function it_can_read_the_license_file()
	{
		$app = new Illuminate\Container\Container;

		$bag = m::mock('Cartalyst\Extensions\ExtensionBag');

		$platform = new Platform($app, $bag);

		$app['files'] = m::mock('Illuminate\Filesystem\Filesystem');
		$app['files']->shouldReceive('exists')->with('/\/license\.txt$/')->once()->andReturn(true);
		$app['files']->shouldReceive('get')->with('/\/license\.txt$/')->once()->andReturn('foo');

		$this->assertEquals('foo', $platform->getLicense());
	}

	/**
	 * @test
	 * @expectedException RuntimeException
	 */
	public function it_throws_an_exception_if_the_license_file_does_not_exist()
	{
		$app = new Illuminate\Container\Container;

		$bag = m::mock('Cartalyst\Extensions\ExtensionBag');

		$platform = new Platform($app, $bag);

		$app['files'] = m::mock('Illuminate\Filesystem\Filesystem');
		$app['files']->shouldReceive('exists')->with('/\/license\.txt$/')->once()->andReturn(false);

		$platform->getLicense();
	}

}
