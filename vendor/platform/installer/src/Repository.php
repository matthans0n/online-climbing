<?php namespace Platform\Installer;
/**
 * Part of the Platform Installer extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Installer extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Validation\Factory as ValidationFactory;

class Repository {

	/**
	 * The repository's validation factory object.
	 *
	 * @var Illuminate\Validation\Factory
	 */
	protected $validationFactory;

	/**
	 * The database configuration for each database driver. This is because
	 * each database has different configuration options available. The
	 * configurations below match up with the default configurations
	 * specified by Laravel.
	 *
	 * Also, you'll notice the PDO driver prefix for each database configuration
	 * is skipped. This is because we are going to assume the driver matches
	 * the connection name. If somebody wants customization on this, they
	 * can go and edit their database.php file themselves.
	 *
	 * @var array
	 */
	protected $databaseConfig = [

		'sqlite' => [
			'database' => '',
			'prefix'   => '',
		],

		'mysql' => [
			'host'      => 'localhost',
			'database'  => '',
			'username'  => '',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		],

		'pgsql' => [
			'host'     => 'localhost',
			'database' => '',
			'username' => '',
			'password' => '',
			'charset'  => 'utf8',
			'prefix'   => '',
			'schema'   => 'public',
		],

		'sqlsrv' => [
			'host'     => 'localhost',
			'database' => '',
			'username' => '',
			'password' => '',
			'prefix'   => '',
		],

	];

	/**
	 * The rules to match the database configuration above. This helps us
	 * validate the configuration provided to this repository before we
	 * even attempt to install Platform.
	 *
	 * @var array
	 */
	protected $databaseRules = [

		'sqlite' => [
			'database' => 'required',
		],

		'mysql' => [
			'host'      => 'required',
			'database'  => 'required',
			'username'  => 'required',
			'charset'   => 'required',
			'collation' => 'required',
		],

		'pgsql' => [
			'host'     => 'required',
			'database' => 'required',
			'username' => 'required',
		],

		'sqlsrv' => [
			'host'     => 'required',
			'database' => 'required',
			'username' => 'required',
		],
	];

	/**
	 * Holds the chosen database driver.
	 *
	 * @var string
	 */
	protected $databaseDriver;

	/**
	 * Holds the user configuration data.
	 *
	 * @var array
	 */
	protected $userConfig = [
		'email'            => '',
		'password'         => '',
		'password_confirm' => '',
	];

	/**
	 * Array of validation rules for the user config.
	 *
	 * @var array
	 */
	protected $userRules = [
		'email'            => 'required|email',
		'password'         => 'required|min:6',
		'password_confirm' => 'required|same:password',
	];

	/**
	 * Initializes a new installer repository.
	 *
	 * @param  Illuminate\Validation\Factory  $validation
	 * @param  array  $databaseConfig
	 * @param  array  $userConfig
	 * @param  array  $databaseRules
	 * @param  array  $userRules
	 * @return void
	 */
	public function __construct(
		ValidationFactory $validationFactory,
		array $databaseConfig = null,
		array $userConfig = null,
		array $databaseRules = null,
		array $userRules = null
	)
	{
		$this->validationFactory = $validationFactory;

		if (isset($databaseConfig))
		{
			$this->databaseConfig = $databaseConfig;
		}

		if (isset($userConfig))
		{
			$this->userConfig = $userConfig;
		}

		if (isset($databaseRules))
		{
			$this->databaseRules = $databaseRules;
		}

		if (isset($userRules))
		{
			$this->userRules = $userRules;
		}
	}

	/**
	 * Gets the available database drivers.
	 *
	 * @return array
	 */
	public function getDatabaseDrivers()
	{
		return array_keys($this->databaseConfig);
	}

	/**
	 * Sets the chosen database driver.
	 *
	 * @param  string  $driver
	 * @return void
	 * @throws RuntimeException
	 */
	public function setDatabaseDriver($driver)
	{
		// We don't support adding new drivers
		if ( ! isset($this->databaseConfig[$driver]))
		{
			throw new \RuntimeException("Database configuration does not exist for driver [$driver].");
		}

		$this->databaseDriver = $driver;
	}

	/**
	 * Gets the database driver.
	 *
	 * @return string
	 */
	public function getDatabaseDriver()
	{
		return $this->databaseDriver;
	}

	/**
	 * Sets the database configuration for a driver.
	 *
	 * @param  string  $driver
	 * @param  array   $config
	 * @return void
	 * @throws RuntimeException
	 */
	public function setDatabaseConfig($driver, array $config)
	{
		// We don't support adding new drivers
		if ( ! isset($this->databaseConfig[$driver]))
		{
			throw new \RuntimeException("Database configuration does not exist for driver [$driver].");
		}

		$this->databaseConfig[$driver] = $config;
	}

	/**
	 * Merges the database configuration for a driver.
	 *
	 * @param  string  $driver
	 * @param  array   $config
	 * @return void
	 * @throws RuntimeException
	 */
	public function mergeDatabaseConfig($driver, array $config)
	{
		// We don't support adding new drivers
		if ( ! isset($this->databaseConfig[$driver]))
		{
			throw new \RuntimeException("Database configuration does not exist for driver [$driver].");
		}

		$this->databaseConfig[$driver] = array_merge(
			$this->databaseConfig[$driver],
			$config
		);
	}

	/**
	 * Gets the database configuration for a driver.
	 *
	 * @return array  $driver
	 * @throws RuntimeException
	 */
	public function getDatabaseConfig($driver = null)
	{
		// Default driver
		$driver = $driver ?: $this->getDatabaseDriver();

		// We don't support adding new drivers
		if ( ! isset($this->databaseConfig[$driver]))
		{
			throw new \RuntimeException("Database configuration does not exist for driver [$driver].");
		}

		return $this->databaseConfig[$driver];
	}

	/**
	 * Returns the validator for the database
	 * configuration.
	 *
	 * @param  string  $driver
	 * @return Illuminate\Validation\Validator
	 * @throws RuntimeException
	 */
	public function getDatabaseConfigValidator($driver = null)
	{
		// Default driver
		$driver = $driver ?: $this->getDatabaseDriver();

		// We don't support adding new drivers
		if ( ! isset($this->databaseConfig[$driver]))
		{
			throw new \RuntimeException("Database configuration does not exist for driver [$driver].");
		}

		return $this->validationFactory->make($this->databaseConfig[$driver], $this->databaseRules[$driver]);
	}

	/**
	 * Returns the user configuration.
	 *
	 * @return array
	 */
	public function getUserConfig()
	{
		return $this->userConfig;
	}

	/**
	 * Sets teh user configuration.
	 *
	 * @return void
	 */
	public function setUserConfig(array $config)
	{
		$this->userConfig = $config;
	}

	/**
	 * Merges the user config with the provided config.
	 *
	 * @return void
	 */
	public function mergeUserConfig(array $config)
	{
		$this->userConfig = array_merge(
			$this->userConfig,
			$config
		);
	}

	/**
	 * Returns the validator for the user configuration.
	 *
	 * @return Illuminate\Validation\Validator
	 */
	public function getUserConfigValidator()
	{
		return $this->validationFactory->make($this->userConfig, $this->userRules);
	}

}
