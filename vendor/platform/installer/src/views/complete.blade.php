@extends('platform/installer::template')

{{-- Installer title --}}
@section('title')
@parent
:: Complete
@stop

{{-- Installer content --}}
@section('content')

<div class="row">

	<div class="small-1 columns"></div>

	<div class="small-8 columns">

		<div class="row">
			<h1>Installation Successful</h1>
			<h2>Code Well, Rock On</h2>
		</div>

		<hr>

		<div class="row">
			<pre class="license">{{ Platform::getLicense() }}</pre>
		</div>

		<hr>

		<div class="row">
			<a href="{{ URL::to('/') }}" class="button button-install">
				I Agree
			</a>
		</div>

	</div>

	<div class="small-3 columns"></div>

</div>

@stop
