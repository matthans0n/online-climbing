@extends('platform/installer::template')

{{-- Installer title --}}
@section('title')
@parent
:: Configuration
@stop

{{-- Inline scripts --}}
@section('scripts')

<script>

	$(document).foundation();

</script>

@stop

{{-- Installer content --}}
@section('content')

<form class="installer" method="POST" action="{{ URL::to('installer/configure') }}" data-abide>

	<div class="row">

		<div class="small-1 columns"></div>

		<div class="small-8 columns">

			<h1>PLATFORM</h1>

			<h2>A flexible and extensible web application.</h2>

			<hr>

			<ul class="button-group">

				@foreach ($requirements as $requirement)
				<li><a title="{{ $requirement->message }}" class="button small secondary success {{ $requirement->check }}"><i class="fa @if ($requirement->check) fa-square-o @else fa-check-square-o @endif"></i> {{ $requirement->title }}</a></li>
				@endforeach

				@if ( ! $pass)
				<li><a href="{{ URL::to('/') }}" class="button small button-install"><i class="fa fa-refresh"></i></a></li>
				@endif

			</ul>

			@if ($errors->count())
				@foreach ($errors->all(':message') as $error)
				<p class="notify">
					{{ $error }}
				</p>
				@endforeach
			@endif

			@if ($pass)
			<div class="row">

				<div class="large-12 columns">
					<input type="email" name="user[email]" value="{{{ Input::old('user.email') }}}" placeholder="Email Address" required>

					<small class="error">Enter a valid email address.</small>
				</div>

				<div class="large-12 columns">
					<input type="password" id="password" name="user[password]" value="{{{ Input::old('user.password') }}}" placeholder="Password" required>

					<small class="error">Choose a strong password.</small>
				</div>

				<div class="large-12 columns">
					<input type="password" id="confirmPassword" name="user[password_confirm]" value="{{{ Input::old('user.password_confirm') }}}" placeholder="Confirm Password" required data-equalto="password">

					<small class="error">Passwords must match.</small>
				</div>

				<div class="large-12 columns">

					<div class="row collapse">

						<select name="database[driver]" id="choose-database-driver" class="medium" required>
							<option class="default" value="">Database Driver</option>
							<optgroup label="Available Drivers">
								@foreach ($databaseConfig as $driver => $driverConfig)
								<option value="{{ $driver }}" {{ (Input::old('database.driver') == $driver) ? 'selected' : '' }}>
									{{ $driver }}
								</option>
								@endforeach
							</optgroup>
						</select>

						<small class="error">Select a database driver.</small>

					</div>

				</div>

				@foreach ($databaseConfig as $driver => $driverConfig)
				<div class="database-driver hide" id="database-driver-{{ $driver }}">

					@foreach ($driverConfig as $index => $value)
					<div class="large-12 columns">
						<input type="{{ $index == 'password' ? 'password' : 'text' }}" name="database[{{ $driver }}][{{ $index }}]" placeholder="{{ $index }}" value="{{{ Input::old("database.{$driver}.{$index}", $value) }}}">
					</div>
					@endforeach

				</div>
				@endforeach

				<div class="large-12 columns">
					<button type="submit" class="button button-install">
						Install Platform<br><small>v2.0.0 RC4 Ornery Octopus</small>
					</button>
				</div>

			</div>
			@endif

		</div>

		<div class="small-3 columns"></div>

	</div>

</form>

@stop
