<!DOCTYPE html>
<html>
	<head>
		<title>
			@section('title')
			Platform Installer
			@show
		</title>

		<link href="{{ URL::to('packages/platform/installer/css/normalize.css') }}" rel="stylesheet">
		<link href="{{ URL::to('packages/platform/installer/css/foundation.min.css') }}" rel="stylesheet">
		<link href="{{ URL::to('packages/platform/installer/css/font-awesome.min.css') }}" rel="stylesheet">
		<link href="{{ URL::to('packages/platform/installer/css/install.css') }}" rel="stylesheet">

		<script src="{{ URL::to('packages/platform/installer/js/modernizr.js') }}"></script>

		@section('styles')
		@show
	</head>
	<body>

		<div class="loader">
			<div>
				<span><img src="{{ URL::to('packages/platform/installer/img/loader.gif') }}" alt=""><br>Installing</span>
			</div>
		</div>

		<div class="base">

			@yield('content')

		</div>

		<script src="{{ URL::to('packages/platform/installer/js/jquery.js') }}"></script>
		<script src="{{ URL::to('packages/platform/installer/js/foundation.min.js') }}"></script>
		<script src="{{ URL::to('packages/platform/installer/js/install.js') }}"></script>

		@section('scripts')
		@show

	</body>

</html>
