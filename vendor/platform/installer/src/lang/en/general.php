<?php

return [

	'php_title'   => 'PHP Version',
	'php_message' => 'PHP version 5.4 or greater required.',

	'gd_title'   => 'GD',
	'gd_message' => 'GD extension is required.',

	'mcrypt_title'   => 'Mcrypt',
	'mcrypt_message' => 'Mcrypt extension is required.',

	'permissions_title'   => 'Write Permissions',
	'permissions_message' => 'app/config/app.php, app/storage &amp; public/cache must be writable.',

];
