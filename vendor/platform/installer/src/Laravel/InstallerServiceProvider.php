<?php namespace Platform\Installer\Laravel;
/**
 * Part of the Platform Installer extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Installer extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Support\ServiceProvider;
use Platform\Installer\Console\InstallCommand;
use Platform\Installer\Installer;
use Platform\Installer\Repository;
use Redirect;

class InstallerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('platform/installer', 'platform/installer', __DIR__.'/..');

		$this->trustIps();

		$this->whitelistOperations();

		$this->loadRoutes();

		$this->observeEvents();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerRepository();

		$this->registerInstaller();

		$this->registerCommands();
	}

	/**
	 * Registers the installer repository which is used when installing Platform.
	 *
	 * @return void
	 */
	protected function registerRepository()
	{
		$this->app['platform.installer.repository'] = $this->app->share(function($app)
		{
			return new Repository($app['validator']);
		});
	}

	/**
	 * Registers the installer.
	 *
	 * @return void
	 */
	protected function registerInstaller()
	{
		$this->app['platform.installer'] = $this->app->share(function($app)
		{
			return new Installer($app, $app['platform'], $app['platform.installer.repository']);
		});
	}

	/**
	 * Registers the install commands.
	 *
	 * @return void
	 */
	protected function registerCommands()
	{
		$this->app['command.platform.install'] = $this->app->share(function($app)
		{
			return new InstallCommand($app['platform.installer']);
		});
	}

	/**
	 * Adds the trusted IPs to Platform.
	 *
	 * @return void
	 */
	protected function trustIps()
	{
		foreach ($this->app['config']['platform/installer::trusted_ips'] as $trustedIp)
		{
			$this->app['platform']->trustIp($trustedIp);
		}
	}

	/**
	 * When Platform isn't eligible to run (not installed usually),
	 * it only allows a certain range of URIs to be hit. This is for
	 * security. We will allow the installer URIs to be hit so
	 * Platform can be installed.
	 *
	 * @return void
	 */
	protected function whitelistOperations()
	{
		foreach ($this->app['config']['platform/installer::eligibility_whitelist'] as $uri)
		{
			$this->app['platform']->addEligibilityWhitelist($uri);
		}
	}

	/**
	 * Observe various events.
	 *
	 * @return void
	 */
	protected function observeEvents()
	{
		$app = $this->app;

		// Add artisan events
		$this->app['events']->listen('artisan.start', function($artisan) use ($app)
		{
			if ( ! $app['platform']->isInstalled())
			{
				$artisan->resolveCommands(['command.platform.install']);
			}
		});

		// If Platform is ineligible to run, we'll redirect to our route which is
		// whitelisted for elligiblity where we will handle the install process.
		$this->app['events']->listen('platform.ineligible', function($platform) use ($app)
		{
			$app->before(function()
			{
				return Redirect::to('installer');
			});
		});
	}

	/**
	 * Load the routes file for the operations web UI.
	 *
	 * @return void
	 */
	protected function loadRoutes()
	{
		$result = require_once __DIR__.'/../routes.php';
	}

}
