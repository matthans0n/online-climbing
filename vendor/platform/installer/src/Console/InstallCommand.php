<?php namespace Platform\Installer\Console;
/**
 * Part of the Platform Installer extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Installer extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Console\Command;
use Platform\Installer\Installer;
use Symfony\Component\Console\Input\InputOption;

class InstallCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'platform:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Install Platform';

	/**
	 * The installer assosiated with the command.
	 *
	 * @var Platform\Foundation\Install\Installer
	 */
	protected $installer;

	/**
	 * The repository associated with the installer.
	 *
	 * @var Platform\Foundation\Install\Repository
	 */
	protected $repository;

	/**
	 * Create a new install command instance
	 *
	 * @param  Platform\Foundation\Install\Installer  $installer
	 * @return void
	 */
	public function __construct(Installer $installer)
	{
		parent::__construct();

		$this->installer  = $installer;
		$this->repository = $this->installer->getRepository();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		// Build up a nice welcome message
		$this->info(<<<WELCOME

*-----------------------------------------------*
|                                               |
|       Welcome to the Platform Installer       |
|            Copyright (c) 2011-2014            |
|                 Cartalyst LLC.                |
|                                               |
|         Platform is release under the         |
|                Cartalyst PSL                  |
|         https://cartalyst.com/license         |
|          Thanks for using Platform!           |
|                                               |
*-----------------------------------------------*

                   _          _              _
  ___  __ _  _ __ | |_  __ _ | | _   _  ___ | |_
 / __|/ _` || '__|| __|/ _` || || | | |/ __|| __|
| (__| (_| || |   | |_| (_| || || |_| |\__ \| |_
 \___|\__,_||_|    \__|\__,_||_| \__, ||___/ \__|
                                 |___/

WELCOME
		);

		// Setup counte
		$stepCounter = 1;

		// If we're not skipping configuration
		if ( ! $this->input->getOption('skip-config')) {

			// First step, configure database
			$this->info(sprintf(<<<STEP

*-----------------------------------------------*
|                                               |
|         Step #%d: Configure Database           |
|                                               |
*-----------------------------------------------*

STEP
			, $stepCounter++));

			// Ask for the database configuration
			$this->askDatabaseDriver();
			$this->setupDatabaseConfig();

			// Here is where you would place extra config
			// @todo, maybe an an event which fires???


			$this->comment('Thanks, that configuration looks acceptable.');
		}

		// If we're not skipping configuration
		if ( ! $this->input->getOption('skip-user')) {

			// First step, configure database
			$this->info(sprintf(<<<STEP

*-----------------------------------------------*
|                                               |
|       Step #%d: Configure Default User         |
|                                               |
*-----------------------------------------------*

STEP
			, $stepCounter++));

			// Ask for the user configuration
			$this->setupUserConfig();

			$this->comment('Thanks, that configuration looks acceptable.');
		}

		$this->comment('Great, we have all the information we need. We\'ll proceed with the actual install process now.');

		try
		{
			$this->installer->callBeforeEvents();

			$this->installer->generateAppKey();

			if ( ! $this->input->getOption('skip-config'))
			{
				$this->installer->setupDatabase();
			}

			if ( ! $this->input->getOption('skip-extensions'))
			{
				$this->installer->migrateRequiredPackages();
				$this->installer->migrateFoundation();
				$this->installer->installExtensions();
			}

			if ( ! $this->input->getOption('skip-user'))
			{
				$this->installer->createDefaultUser();
			}

			$this->installer->callAfterEvents();

			$this->installer->updatePlatformInstalledVersion($this->laravel['platform']->codebaseVersion());
		}
		catch (\Exception $e)
		{
			$this->error('An error occured during installation:');
			throw $e;
		}

		$this->comment('Installation complete.');
	}

	/**
	 * Sets up the database config in the repository
	 * based off a number of questions asked of the user.
	 *
	 * @return void
	 */
	protected function setupDatabaseConfig()
	{
		// Get the database driver
		$driver = $this->repository->getDatabaseDriver();

		// Set the databse config based off what
		// we ask the user
		$this->repository->setDatabaseConfig(
			$driver,
			$this->askNewConfig($this->repository->getDatabaseConfig(), 'database')
		);

		// Setup a loop to ask and validate database configuration
		do
		{
			// Grab the database validator from the install repo
			$validator = $this->repository->getDatabaseConfigValidator();

			// If validation fails, let's just return the
			// messages in a list form.
			if ($validator->fails())
			{
				// Didn't validate
				$proceed = false;

				// Show errors
				$this->error('Validation failed for database configuration.');
				foreach ($validator->messages()->all(':message') as $message)
				{
					$this->error($message);
				}

				// Check they want to try again
				if ( ! $this->confirm('Would like to try again? [y, n]'))
				{
					throw new \RuntimeException('Install aborted by user.');
				}

				// Ask for new configuration for the failed items
				$this->repository->mergeDatabaseConfig($driver, $this->askNewConfig(
					$this->repository->getDatabaseConfig(),
					'database',
					array_keys($validator->messages()->getMessages())
				));
			}
			else
			{
				// Let's get the config as provided
				$databaseConfig = $this->repository->getDatabaseConfig();

				// Get the maximum length of a key in the
				// array so we can format everything nicely
				$maxLength = max(array_map('strlen', array_keys($databaseConfig)));

				// We'll format it nicely
				array_walk($databaseConfig, function(&$value, $key) use ($maxLength)
				{
					$secret = ($key === 'password' or $key === 'password_confirm');

					// +3 because of the "[]:"
					$key = str_pad("[$key]:", $maxLength + 3);

					$value = $secret ? "$key ******" : "$key $value";
				});

				// Let's confirm with the person
				$this->comment(sprintf(
					"You have provided the following configuration:\n%s",
					implode("\n", $databaseConfig)
				));

				// Don't want to proceed? We'll start again
				if ( ! $this->confirm('Are you happy to proceed? [y, n]:'))
				{
					// Let's reset the config.
					$this->repository->setDatabaseConfig($driver, array_map(function($config)
					{
						return '';
					}, $this->repository->getDatabaseConfig()));

					// Recursive baby
					$this->setupDatabaseConfig();
				}

				// Did validate, this'll break
				// the loop
				$proceed = true;
			}
		}
		while ( ! $proceed);
	}

	/**
	 * Gets (and sets in the repository) the
	 * database driver, as specified by the
	 * user.
	 *
	 * @return string
	 */
	protected function askDatabaseDriver()
	{
		// Get valid drivers
		$drivers = $this->repository->getDatabaseDrivers();

		do
		{
			// Get driver
			$driver = $this->ask(sprintf(
				'Please enter database driver [%s]:',
				implode(', ', $drivers)
			));

			// Invalid driver?
			if ( ! in_array($driver, $drivers))
			{
				$this->error("Driver [$driver] is not a valid driver. Please try again.");

				// Keep our loop going
				$driver = false;
			}

			// Valid driver
			else
			{
				// If the person cancels out
				if ( ! $this->confirm("Are you sure you want to use the [$driver] database driver? [y, n]:"))
				{
					$driver = false;
				}
			}
		}
		while ( ! $driver);

		// Set in the repository
		$this->repository->setDatabaseDriver($driver);

		return $driver;
	}

	/**
	 * Sets up the user for their configuration information
	 * and checks that it's acceptable.
	 *
	 * @return void
	 */
	protected function setupUserConfig()
	{
		$this->repository->setUserConfig(
			$this->askNewConfig($this->repository->getUserConfig(), 'default user')
		);

		do
		{
			// Grab the user validator from the install repo
			$validator = $this->repository->getUserConfigValidator();

			// If validation fails, let's just return the
			// messages in a list form.
			if ($validator->fails())
			{
				// Didn't validate
				$proceed = false;

				// Show errors
				$this->error('Validation failed for user configuration.');
				foreach ($validator->messages()->all(':message') as $message)
				{
					$this->error($message);
				}

				// Check they want to try again
				if ( ! $this->confirm('Would like to try again? [y, n]'))
				{
					throw new \RuntimeException('Install aborted by user.');
				}

				// There is a special rule for user configuration with passwords
				// and confirmations
				$keysToAsk = array_keys($validator->messages()->getMessages());
				if (in_array('password', $keysToAsk) or in_array('password_confirm', $keysToAsk))
				{
					if (($key = array_search('password', $keysToAsk)) !== false)
					{
						unset($keysToAsk[$key]);
					}
					if (($key = array_search('password_confirm', $keysToAsk)) !== false)
					{
						unset($keysToAsk[$key]);
					}
					$keysToAsk[] = 'password';
					$keysToAsk[] = 'password_confirm';
				}

				// Ask for new configuration for the failed items
				$this->repository->mergeUserConfig($this->askNewConfig(
					$this->repository->getUserConfig(),
					'default user',
					$keysToAsk
				));
			}
			else
			{
				// Let's get the config as provided
				$userConfig = $this->repository->getUserConfig();

				// Get the maximum length of a key in the
				// array so we can format everything nicely
				$maxLength = max(array_map('strlen', array_keys($userConfig)));

				// We'll format it nicely
				array_walk($userConfig, function(&$value, $key) use ($maxLength)
				{
					$secret = ($key === 'password' or $key === 'password_confirm');

					// +3 because of the "[]:"
					$key = str_pad("[$key]:", $maxLength + 3);

					$value = $secret ? "$key ******" : "$key $value";
				});

				// Let's confirm with the person
				$this->comment(sprintf(
					"You have provided the following configuration:\n%s",
					implode("\n", $userConfig)
				));

				// Don't want to proceed? We'll start again
				if ( ! $this->confirm('Are you happy to proceed? [y, n]:'))
				{
					// Let's reset the config.
					$this->repository->setUserConfig(array_map(function($config)
					{
						return '';
					}, $this->repository->getUserConfig()));

					// Recursive baby
					$this->setupUserConfig();
				}

				// Did validate, this'll break
				// the loop
				$proceed = true;
			}
		}
		while ( ! $proceed);
	}

	/**
	 * Asks for new configuration based off the old configuration.
	 *
	 * The second parameter is an array of keys from the existing
	 * config to query the user for.
	 *
	 * @param  array  $existingConfig
	 * @param  string $typeDescription
	 * @param  array  $keys
	 * @return array
	 */
	protected function askNewConfig($existingConfig, $typeDescription = null, array $keys = array('*'))
	{
		$newConfig = array();

		// All keys?
		if ((count($keys) === 1) and (reset($keys) === '*'))
		{
			$keysToAsk = array_keys($existingConfig);
		}

		// Specified keys
		else
		{
			$keysToAsk = $keys;
		}

		// Let's go through the first time and ask
		// for the configuration
		foreach ($keysToAsk as $key)
		{
			$value = $existingConfig[$key];
			$secret = ($key === 'password' or $key === 'password_confirm');
			$newConfig[$key] = $this->askConfig($key, $value, $typeDescription, $secret);
		}

		return $newConfig;
	}

	/**
	 * Asks for a configuration item with the given key,
	 * and the current value. The user can enter through
	 * for the current configuration to be used.
	 *
	 * @param  string  $key
	 * @param  string  $current
	 * @param  string  $typeDescription
	 * @return string
	 */
	protected function askConfig($key, $current, $typeDescription = null, $secret)
	{
		if ($secret)
		{
			return $this->secret(sprintf(
				'Please enter%s [%s]:',
				($typeDescription) ? " {$typeDescription}" : '',
				$key
			), $current);
		}
		else
		{
			return $this->ask(sprintf(
				'Please enter%s [%s] (enter for %s):',
				($typeDescription) ? " {$typeDescription}" : '',
				$key,
				($current) ? "[$current]" : 'blank'
			), $current);
		}
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip-config', null, InputOption::VALUE_NONE, 'Skip config files (setup manually)'),
			array('skip-extensions', null, InputOption::VALUE_NONE, 'Skip installing extensions (setup manually)'),
			array('skip-user', null, InputOption::VALUE_NONE, 'Skip user setup (setup manually)'),
		);
	}

}
