<?php namespace Platform\Installer\Controllers;
/**
 * Part of the Platform Installer extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Installer extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Exception;
use Illuminate\Routing\Controller;
use Illuminate\Support\MessageBag;
use Redirect;
use Request;
use Route;
use View;
use StdClass;
use Lang;
use Illuminate\Filesystem\Filesystem;

class InstallerController extends Controller {

	/**
	 * The shared Platform instance.
	 *
	 * @var Platform\Foundation\Platform
	 */
	protected $platform;

	/**
	 * The shared installer repository.
	 *
	 * @var Platform\Installer\Repository
	 */
	protected $repository;

	/**
	 * The filesystem instance.
	 *
	 * @var \Illuminate\Filesystem\Filesystem
	 */
	protected $files;

	/**
	 * Create a new installer instance.
	 *
	 * @return void
	 */
	public function __construct(Filesystem $files)
	{
		$this->platform   = $platform = app('platform');
		$this->repository = app('platform.installer.repository');
		$this->files = $files;

		Route::filter('installer', function($route, $request) use ($platform)
		{
			$completionStep = ends_with($request->path(), 'complete');
			$installed      = $platform->isInstalled();

			if ($completionStep and ! $installed)
			{
				return Redirect::to('installer');
			}

			if ( ! $completionStep and $installed)
			{
				return Redirect::to('/');
			}
		});

		$this->beforeFilter('installer');
	}

	/**
	 * Default index route, we'll redirect to the configure screen.*
	 *
	 * @return Illuminate\Http\RedirectResponse
	 */
	public function getIndex()
	{
		return Redirect::to('installer/configure');
	}

	/**
	 * Show the "configure" screen.
	 *
	 * @return Illuminate\View\View
	 */
	public function getConfigure()
	{
		$databaseConfig = array();

		foreach ($this->repository->getDatabaseDrivers() as $driver)
		{
			$databaseConfig[$driver] = $this->repository->getDatabaseConfig($driver);
		}

		$php          = new StdClass;
		$php->check   = 'alert';
		$php->title   = Lang::get('platform/installer::general.php_title');
		$php->message = Lang::get('platform/installer::general.php_message');

		if (version_compare(PHP_VERSION, '5.4', '>='))
		{
			$php->check = null;
		}

		$gd          = new StdClass;
		$gd->check   = 'alert';
		$gd->title   = Lang::get('platform/installer::general.gd_title');
		$gd->message = Lang::get('platform/installer::general.gd_message');

		if (extension_loaded('gd'))
		{
			$gd->check = null;
		}

		$mcrypt          = new StdClass;
		$mcrypt->check   = 'alert';
		$mcrypt->title   = Lang::get('platform/installer::general.mcrypt_title');
		$mcrypt->message = Lang::get('platform/installer::general.mcrypt_message');

		if (extension_loaded('mcrypt'))
		{
			$mcrypt->check = null;
		}

		$permissions          = new StdClass;
		$permissions->check   = 'alert';
		$permissions->title   = Lang::get('platform/installer::general.permissions_title');
		$permissions->message = Lang::get('platform/installer::general.permissions_message');

		if ($this->files->isWritable(storage_path()) && $this->files->isWritable(public_path().'/cache') && $this->files->isWritable(app_path().'/config'))
		{
			$permissions->check = null;
		}

		$requirements = [
			$php,
			$gd,
			$mcrypt,
			$permissions,
		];

		$pass = true;

		foreach ($requirements as $requirement)
		{
			if ($requirement->check)
			{
				$pass = false;

				break;
			}
		}

		return View::make('platform/installer::configure', compact('databaseConfig', 'requirements', 'pass'));
	}

	/**
	 * Handle the configuration and installation of Platform.
	 *
	 * @return mixed
	 */
	public function postConfigure()
	{
		try
		{
			$skipConfig     = Request::input('skip_config');
			$skipExtensions = Request::input('skip_extensions');
			$skipUser       = Request::input('skip_user');

			// Grab and validate user config
			if ( ! $skipUser)
			{
				$this->repository->mergeUserConfig(Request::input('user', array()));

				$validator = $this->repository->getUserConfigValidator();

				if ($validator->fails())
				{
					return $this->tryAgain($validator->errors());
				}
			}

			// Grab and validate database config
			if ( ! $skipConfig)
			{
				if ( ! $driver = Request::input('database.driver'))
				{
					throw new \RuntimeException('Please specify a database driver.');
				}

				$this->repository->setDatabaseDriver($driver);
				$this->repository->mergeDatabaseConfig($driver, Request::input("database.$driver", array()));

				$validator = $this->repository->getDatabaseConfigValidator();

				if ($validator->fails())
				{
					return $this->tryAgain($validator->errors());
				}
			}

			$installer = app('platform.installer');

			$installer->callBeforeEvents();

			$installer->generateAppKey();

			// Install config
			if ( ! $skipConfig)
			{
				$installer->setupDatabase();
			}

			if ( ! $skipExtensions)
			{
				$installer->migrateRequiredPackages();
				$installer->migrateFoundation();
				$installer->installExtensions();
			}

			if ( ! $skipUser)
			{
				$installer->createDefaultUser();
			}

			$installer->callAfterEvents();

			$installer->updatePlatformInstalledVersion($this->platform->codebaseVersion());

			return Redirect::to('installer/complete');
		}
		catch (Exception $e)
		{
			$error = $e->getMessage();
			return $this->tryAgain(compact('error'));
		}
	}

	/**
	 * Show the "complete" step.
	 *
	 * @return Illuminate\View\View
	 */
	public function getComplete()
	{
		return View::make('platform/installer::complete');
	}

	/**
	 * Sends the user back to the configure screen
	 * and attaches the errors to the session.
	 *
	 * @param  mixed  $errors
	 * @return Illuminate\Http\RedirectResponse
	 */
	protected function tryAgain($errors)
	{
		if (is_array($errors))
		{
			$errors = new MessageBag($errors);
		}

		return Redirect::to('installer/configure')
			->withErrors($errors)
			->withInput();
	}

}
