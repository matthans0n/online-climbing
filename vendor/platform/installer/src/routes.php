<?php
/**
 * Part of the Platform Installer extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Installer extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

Route::group(['prefix' => 'installer', 'namespace' => 'Platform\Installer\Controllers'], function()
{
	Route::get('/', 'InstallerController@getIndex');
	Route::get('configure', 'InstallerController@getConfigure');
	Route::post('configure', 'InstallerController@postConfigure');
	Route::get('complete', 'InstallerController@getComplete');
});
