<?php
/**
 * Part of the Platform Installer extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Installer extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Mockery as m;
use Platform\Installer\Repository;

class PlatformRepositoryTest extends PHPUnit_Framework_TestCase {

	protected $validator;

	/**
	 * Setup resources and dependencies.
	 *
	 * @return void
	 */
	public function setUp()
	{
		$this->validator = m::mock('Illuminate\Validation\Factory');
	}

	/**
	 * Close mockery.
	 *
	 * @return void
	 */
	public function tearDown()
	{
		m::close();
	}

	public function testRepoSetsDatabaseDriver()
	{
		$repo = new Repository($this->validator);

		$repo->setDatabaseDriver('mysql');

		$this->assertEquals('mysql', $repo->getDatabaseDriver());
	}

	/**
	 * @expectedException RuntimeException
	 */
	public function testRepoThrowsExceptionWhenNoDriverIsSpecifiedAndThereIsNoDefaultForDatabaseConfig()
	{
		$repo = new Repository(
			$this->validator,
			array(
				'mysql' => array(
					'foo' => 'bar',
					'baz' => 'cux',
				),
			)
		);

		$repo->getDatabaseConfig();
	}

	public function testRepoReturnsDefaultDatabaseConfig()
	{
		$repo = new Repository(
			$this->validator,
			array(
				'mysql' => array(
					'foo' => 'bar',
					'baz' => 'qux',
				),
			)
		);

		$expected = array(
			'foo' => 'bar',
			'baz' => 'qux',
		);

		$repo->setDatabaseDriver('mysql');

		$this->assertEquals($expected, $repo->getDatabaseConfig());
	}

	/**
	 * @expectedException RuntimeException
	 */
	public function testRepoDoesNotAllowAddingNewDatabaseDrivers()
	{
		$repo = new Repository(
			$this->validator,
			array(
				'mysql' => array(
					'foo' => 'bar',
					'baz' => 'qux',
				),
			)
		);

		$repo->setDatabaseConfig('fakedriver', array());
	}

	public function testRepoMergesDatabaseConfig()
	{
		$repo = new Repository(
			$this->validator,
			array(
				'mysql' => array(
					'foo' => 'bar',
					'baz' => 'qux',
				),
			)
		);

		$repo->mergeDatabaseConfig('mysql', array(
			'fred' => 'corge',
			'foo'  => '',
		));

		$expected = array(
			'foo'  => '',
			'baz'  => 'qux',
			'fred' => 'corge',
		);

		$this->assertEquals($expected, $repo->getDatabaseConfig('mysql'));
	}

	public function testRepoSetsUserConfig()
	{
		$repo = new Repository(
			$this->validator,
			array(),
			array(
				'email'    => '',
				'password' => '',
			)
		);

		$expected = array(
			'email'    => '',
			'password' => '',
		);

		$this->assertEquals($expected, $repo->getUserConfig());

		$repo->setUserConfig(array(
			'foo' => 'bar',
			'baz' => 'qux',
		));

		$expected = array(
			'foo' => 'bar',
			'baz' => 'qux',
		);

		$this->assertEquals($expected, $repo->getUserConfig());
	}

	public function testRepoMergesUserConfig()
	{
		$repo = new Repository(
			$this->validator,
			array(),
			array(
				'email'    => '',
				'password' => '',
			)
		);

		$expected = array(
			'email'    => 'foo@bar.com',
			'password' => '',
		);

		$repo->mergeUserConfig(array('email' => 'foo@bar.com'));

		$this->assertEquals($expected, $repo->getUserConfig());

		$expected = array(
			'email'    => '',
			'password' => '',
		);

		$repo->mergeUserConfig(array('email' => ''));

		$this->assertEquals($expected, $repo->getUserConfig());
	}

}
