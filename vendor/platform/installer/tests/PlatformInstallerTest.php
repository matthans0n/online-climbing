<?php
/**
 * Part of the Platform Installer extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Platform Installer extension
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Mockery as m;
use Illuminate\Container\Container;
use Platform\Installer\Installer;

class PlatformInstallerTest extends PHPUnit_Framework_TestCase {

	/**
	 * Close mockery.
	 *
	 * @return void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * This test is a suite of regular expression tests against
	 * the config value replacement function. We throw a bunch of
	 * scenarios at it to see if we can break it.
	 *
	 * @return void
	 */
	public function testInstallerReplacesConfigStringValuesCorrectly()
	{
		$installer  = new Installer(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$string = <<<STRING
	'foo_bar' => null,
STRING;

		$expected = <<<STRING
	'foo_bar' => '2.0.0',
STRING;

		$this->assertEquals(
			$expected,
			$installer->replaceConfigStringValue($string, 'foo_bar', '2.0.0')
		);

		$string = <<<STRING
	'foo_bar' => null,
STRING;

		$expected = <<<STRING
	'foo_bar' => false,
STRING;

		$this->assertEquals(
			$expected,
			$installer->replaceConfigStringValue($string, 'foo_bar', false)
		);

		$string = <<<STRING
	'foo_bar' => '2.0.0',
STRING;

		$expected = <<<STRING
	'foo_bar' => true,
STRING;

		$this->assertEquals(
			$expected,
			$installer->replaceConfigStringValue($string, 'foo_bar', true)
		);

		// Really test out the regular expression
		$string = <<<STRING
	'foo_bar' => 's0meReallyLongString-@#)($#@%*&*(@#)U@:LK:::K@J#$#@??SF"DS"<> \'\'\'\'\' ',
STRING;

		$expected = <<<STRING
	'foo_bar' => '2.0.0',
STRING;

		$this->assertEquals(
			$expected,
			$installer->replaceConfigStringValue($string, 'foo_bar', '2.0.0')
		);

		// Really test out the regular expression
		$string = <<<STRING
	'. \ + * ? [ ^ ] $ ( ) { } = ! < > | : -' => 'foo',
STRING;

		$expected = <<<STRING
	'. \ + * ? [ ^ ] $ ( ) { } = ! < > | : -' => '. \ + * ? [ ^ ] $ ( ) { } = ! < > | : -',
STRING;

		$this->assertEquals(
			$expected,
			$installer->replaceConfigStringValue($string, '. \ + * ? [ ^ ] $ ( ) { } = ! < > | : -', '. \ + * ? [ ^ ] $ ( ) { } = ! < > | : -')
		);

		// Now, let's test with different levels of tabbing / spacing
		$string = <<<STRING
	'foo'=>'bar',
STRING;

		$expected = <<<STRING
	'foo' => '2.0.0',
STRING;

		$this->assertEquals(
			$expected,
			$installer->replaceConfigStringValue($string, 'foo', '2.0.0')
		);

		$string = <<<STRING
	'foo'       =>								'bar'     ,
STRING;

		$expected = <<<STRING
	'foo' => '2.0.0',
STRING;

		$this->assertEquals(
			$expected,
			$installer->replaceConfigStringValue($string, 'foo', '2.0.0')
		);

		// Very real scenario
		$string = <<<STRING
<?php

return array(

	'installed_version' => false,

);
STRING;

		$expected = <<<STRING
<?php

return array(

	'installed_version' => '2.0.0',

);
STRING;

		$this->assertEquals(
			$expected,
			$installer->replaceConfigStringValue($string, 'installed_version', '2.0.0')
		);
	}

	public function testMigrationsArePrepared()
	{
		$installer  = new Installer(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$app['migration.repository'] = m::mock('Illuminate\Database\Migrations\MigrationRepositoryInterface');
		$app['migration.repository']->shouldReceive('getLast')->once()->andThrow(new Exception);
		$app['migration.repository']->shouldReceive('createRepository')->once();

		$installer->prepareMigrations();
	}

	public function testMigartionsAreNotDoubleCreatedWhenPrepared()
	{
		$installer  = new Installer(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$app['migration.repository'] = m::mock('Illuminate\Database\Migrations\MigrationRepositoryInterface');
		$app['migration.repository']->shouldReceive('getLast')->once();
		$app['migration.repository']->shouldReceive('createRepository')->never();

		$installer->prepareMigrations();
	}

	public function testFoundationIsMigrated()
	{
		$installer = m::mock('Platform\Installer\Installer[prepareMigrations]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository'),
			array('foo/bar', 'baz/qux')
		);

		$app['path.base'] = __DIR__;

		$migrator = m::mock('Illuminate\Database\Migrations\Migrator');
		$migrator->shouldReceive('run')->with(__DIR__.'/vendor/platform/foundation/src/migrations')->once();
		$app['migrator'] = $migrator;

		$installer->migrateFoundation();
	}

	public function testMigratedPackagesAreMigrated()
	{
		$installer = m::mock('Platform\Installer\Installer[prepareMigrations]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository'),
			array('foo/bar', 'baz/qux')
		);

		$app['path.base'] = __DIR__;

		$installer->shouldReceive('prepareMigrations')->once();

		$migrator = m::mock('Illuminate\Database\Migrations\Migrator');
		$migrator->shouldReceive('run')->with(__DIR__.'/vendor/foo/bar/src/migrations')->once();
		$migrator->shouldReceive('run')->with(__DIR__.'/vendor/baz/qux/src/migrations')->once();
		$app['migrator'] = $migrator;

		$installer->migrateRequiredPackages();
	}

	public function testInstallingExtensions()
	{
		$installer = m::mock('Platform\Installer\Installer[prepareMigrations]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$app['db'] = m::mock('Illuminate\Database\ConnectionResolverInterface');

		$app['migrator'] = $migrator = m::mock('Illuminate\Database\Migrations\Migrator');
		$app['events']   = $events   = m::mock('Illuminate\Events\Dispatcher');

		$platform->shouldReceive('getExtensionBag')->andReturn($extensionBag = m::mock('Cartalyst\Extensions\ExtensionBag'));

		$extensionBag->shouldReceive('findAndRegisterExtensions')->once();
		$extensionBag->shouldReceive('sortExtensions')->once();

		$extension1 = m::mock('Cartalyst\Extensions\ExtensionInterface');
		$extension1->shouldReceive('isInstalled')->once()->andReturn(false);
		$extension1->shouldReceive('install')->once();
		$extension1->shouldReceive('enable')->once();

		$extension2 = m::mock('Cartalyst\Extensions\ExtensionInterface');
		$extension2->shouldReceive('isInstalled')->once()->andReturn(false);
		$extension2->shouldReceive('install')->once();
		$extension2->shouldReceive('enable')->once();

		$extension3 = m::mock('Cartalyst\Extensions\ExtensionInterface');
		$extension3->shouldReceive('isInstalled')->once()->andReturn(true);
		$extensionBag->shouldReceive('all')->once()->andReturn($extensions = array($extension1, $extension2, $extension3));

		$installer->installExtensions();
	}

	public function testUpdatingInstalledVersionWhenConfigDoesNotExist()
	{
		$installer = m::mock('Platform\Installer\Installer[replaceConfigStringValue]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$app['path'] = 'app_path';

		// Firstly, the function should check for the published config
		$app['files'] = $filesystem = m::mock('Illuminate\Filesystem');
		$filesystem->shouldReceive('isDirectory')->with($expectedConfigPath = $app['path'].'/config/packages/platform/foundation')->once()->andReturn(false);
		$filesystem->shouldReceive('exists')->with($expectedConfigFile = $expectedConfigPath.'/config.php')->never();

		// Because it didn't exist, we're publishing it
		$app['config.publisher'] = $configPublisher = m::mock('Illuminate\Foundation\ConfigPublisher');
		$configPublisher->shouldReceive('publishPackage')->with('platform/foundation')->once();

		// THen the installer updates the config value
		$filesystem->shouldReceive('get')->with($expectedConfigFile)->once()->andReturn('success');
		$installer->shouldReceive('replaceConfigStringValue')->with('success', 'installed_version', '2.0.0')->once()->andReturn('woohoo');

		// And of course, putting the contents in the new file
		$filesystem->shouldReceive('put')->with($expectedConfigFile, 'woohoo')->once()->andReturn(true);

		$app['config'] = $config = m::mock('Illuminate\Config\Repository[set]');
		$config->shouldReceive('set')->with('platform/foundation', null)->once();

		$installer->updatePlatformInstalledVersion('2.0.0');
	}

	/**
	 * @expectedException RuntimeException
	 */
	public function testUpdatingInstalledVersionWhenConfigDoesNotExistThrowsExceptionOnError()
	{
		$installer = m::mock('Platform\Installer\Installer[replaceConfigStringValue]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$app['path'] = 'app_path';

		// Firstly, the function should check for the published config
		$app['files'] = $filesystem = m::mock('Illuminate\Filesystem');
		$filesystem->shouldReceive('isDirectory')->with($expectedConfigPath = $app['path'].'/config/packages/platform/foundation')->once()->andReturn(false);
		$filesystem->shouldReceive('exists')->with($expectedConfigFile = $expectedConfigPath.'/config.php')->never();

		// Because it didn't exist, we're publishing it
		$app['config.publisher'] = $configPublisher = m::mock('Illuminate\Foundation\ConfigPublisher');
		$configPublisher->shouldReceive('publishPackage')->with('platform/foundation')->once();

		// THen the installer updates the config value
		$filesystem->shouldReceive('get')->with($expectedConfigFile)->once()->andReturn('success');
		$installer->shouldReceive('replaceConfigStringValue')->with('success', 'installed_version', '2.0.0')->once()->andReturn('woohoo');

		// This should trigger an error to output
		$filesystem->shouldReceive('put')->with($expectedConfigFile, 'woohoo')->once()->andReturn(false);

		$installer->updatePlatformInstalledVersion('2.0.0');
	}

	public function testUpdatingInstalledVersionDoesNotOverwriteExistingConfig()
	{
		$installer = m::mock('Platform\Installer\Installer[replaceConfigStringValue]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$app['path'] = 'app_path';

		// Firstly, the function should check for the published config
		$app['files'] = $filesystem = m::mock('Illuminate\Filesystem');
		$filesystem->shouldReceive('isDirectory')->with($expectedConfigPath = $app['path'].'/config/packages/platform/foundation')->once()->andReturn(true);
		$filesystem->shouldReceive('exists')->with($expectedConfigFile = $expectedConfigPath.'/config.php')->once()->andReturn(true);

		// Because it didn't exist, we're publishing it
		$app['config.publisher'] = $configPublisher = m::mock('Illuminate\Foundation\ConfigPublisher');

		// Because the filesystem showed the config existed, we won't
		// re-publish it.
		$configPublisher->shouldReceive('publishPackage')->never();

		// THen the installer updates the config value
		$filesystem->shouldReceive('get')->with($expectedConfigFile)->once()->andReturn('success');
		$installer->shouldReceive('replaceConfigStringValue')->with('success', 'installed_version', '2.0.0')->once()->andReturn('woohoo');

		// This should trigger an error to output
		$filesystem->shouldReceive('put')->with($expectedConfigFile, 'woohoo')->once()->andReturn(true);

		$app['config'] = $config = m::mock('Illuminate\Config\Repository[set]');
		$config->shouldReceive('set')->with('platform/foundation', null)->once();

		$installer->updatePlatformInstalledVersion('2.0.0');
	}

	/**
	 * @expectedException UnexpectedValueException
	 */
	public function testCreatingDefaultUserThrowsExceptionIfValidationFails()
	{
		$installer = new Installer(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$repository->shouldReceive('getUserConfigValidator')->once()->andReturn($validator = m::mock('Illuminate\Validation\Validator'));
		$validator->shouldReceive('fails')->once()->andReturn(true);
		$validator->shouldReceive('messages')->once()->andReturn($messages = m::mock('Illuminate\Support\MessageBag'));
		$messages->shouldReceive('all')->with(':message')->once()->andReturn(array());

		$installer->createDefaultUser();
	}

	public function testCreatingDefaultUser()
	{
		$installer = new Installer(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$repository->shouldReceive('getUserConfigValidator')->once()->andReturn($validator = m::mock('Illuminate\Validation\Validator'));
		$validator->shouldReceive('fails')->once()->andReturn(false);

		$repository->shouldReceive('getUserConfig')->once()->andReturn($config = array(
			'email'    => 'foo@bar.com',
			'password' => 'baz_bat',
		));

		$app['sentinel'] = $sentinel = m::mock('Cartalyst\Sentinel\Sentinel');

		$sentinel->shouldReceive('getGroupRepository')->once()
			->andReturn($groupProvider = m::mock('Cartalyst\Sentinel\Groups\GroupInterface'));

		$sentinel->shouldReceive('registerAndActivate')
			->andReturn($user = m::mock('Cartalyst\Sentinel\Users\UserInterface'));

		$user->shouldReceive('groups')->once()
			->andReturn($group = m::mock('Cartalyst\Sentinel\Groups\GroupInterface'));

		$groupProvider->shouldReceive('createModel')->once()
			->andReturn($group);

		$groupData = array(
			'slug' 		  => 'admin',
			'name'        => 'Admin',
			'permissions' => array('admin' => true),
		);

		$group->shouldReceive('fill')->with($groupData)->once()->andReturn($group);
		$group->shouldReceive('save')->once();
		$group->shouldReceive('attach')->once()->with($group);

		$installer->createDefaultUser();
	}

	/**
	 * @expectedException UnexpectedValueException
	 */
	public function testSettingUpDatabaseThrowsExceptionIfValidationFails()
	{
		$installer = m::mock('Platform\Installer\Installer[createDatabaseConfig]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$repository->shouldReceive('getDatabaseConfigValidator')->once()->andReturn($validator = m::mock('Illuminate\Validation\Validator'));
		$validator->shouldReceive('fails')->once()->andReturn(true);
		$validator->shouldReceive('messages')->once()->andReturn($messages = m::mock('Illuminate\Support\MessageBag'));
		$messages->shouldReceive('all')->with(':message')->once()->andReturn(array());

		$installer->setupDatabase();
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testSettingUpDatabaseDoesNotCatchExceptions1()
	{
		$installer = m::mock('Platform\Installer\Installer[createDatabaseConfig]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$repository->shouldReceive('getDatabaseConfigValidator')->once()->andReturn($validator = m::mock('Illuminate\Validation\Validator'));
		$validator->shouldReceive('fails')->once()->andReturn(false);

		// Testing connection
		$repository->shouldReceive('getDatabaseDriver')->andReturn('foo');
		$repository->shouldReceive('getDatabaseConfig')->andReturn(array('bar' => 'baz'));

		$app['db.factory'] = $dbFactory = m::mock('Illuminate\Database\Connectors\ConnectionFactory');
		$dbFactory->shouldReceive('make')->with(array(
			'driver' => 'foo',
			'bar'    => 'baz',
		))->once()->andThrow(new InvalidArgumentException);

		$installer->setupDatabase();
	}

	/**
	 * @expectedException PDOException
	 */
	public function testSettingUpDatabaseDoesNotCatchExceptions2()
	{
		$installer = m::mock('Platform\Installer\Installer[createDatabaseConfig]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$repository->shouldReceive('getDatabaseConfigValidator')->once()->andReturn($validator = m::mock('Illuminate\Validation\Validator'));
		$validator->shouldReceive('fails')->once()->andReturn(false);

		// Testing connection
		$repository->shouldReceive('getDatabaseDriver')->andReturn('foo');
		$repository->shouldReceive('getDatabaseConfig')->andReturn(array('bar' => 'baz'));

		$app['db.factory'] = $dbFactory = m::mock('Illuminate\Database\Connectors\ConnectionFactory');
		$dbFactory->shouldReceive('make')->with(array(
			'driver' => 'foo',
			'bar'    => 'baz',
		))->once()->andThrow(new PDOException);

		$installer->setupDatabase();
	}

	public function testSettingUpDatabase()
	{
		$installer = m::mock('Platform\Installer\Installer[createDatabaseConfig]');
		$installer->__construct(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$repository->shouldReceive('getDatabaseConfigValidator')->once()->andReturn($validator = m::mock('Illuminate\Validation\Validator'));
		$validator->shouldReceive('fails')->once()->andReturn(false);

		// Testing connection
		$repository->shouldReceive('getDatabaseDriver')->andReturn('foo');
		$repository->shouldReceive('getDatabaseConfig')->andReturn(array('bar' => 'baz'));

		$app['db.factory'] = $dbFactory = m::mock('Illuminate\Database\Connectors\ConnectionFactory');
		$dbFactory->shouldReceive('make')->with(array(
			'driver' => 'foo',
			'bar'    => 'baz',
		))->once();

		$installer->shouldReceive('createDatabaseConfig')->once();

		$installer->setupDatabase();
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testCreatingDatabaseConfigThrowsExceptionIfStubDoesNotExist()
	{
		$installer = new Installer(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$repository->shouldReceive('getDatabaseDriver')->andReturn('foo');
		$repository->shouldReceive('getDatabaseConfig')->andReturn(array('bar' => 'baz'));

		$app['files'] = $filesystem = m::mock('Illuminate\Filesystem');
		$filesystem->shouldReceive('exists')->with('/stubs\/database\/foo\.php$/')->once()->andReturn(false);

		$installer->createDatabaseConfig();
	}

	/**
	 * @expectedException RuntimeException
	 */
	public function testCreatingDatabaseConfigThrowsExceptionIfConfigCannotBeWritten()
	{
		$installer = new Installer(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$configFileRegex = '/stubs\/database\/foo\.php$/';

		$repository->shouldReceive('getDatabaseDriver')->andReturn('foo');
		$repository->shouldReceive('getDatabaseConfig')->andReturn(array('database' => 'bar'));

		$app['files'] = $filesystem = m::mock('Illuminate\Filesystem');
		$filesystem->shouldReceive('exists')->with($configFileRegex)->once()->andReturn(true);

		$filesystem->shouldReceive('get')->with($configFileRegex)->andReturn(<<<STRING
<?php
return array(
	'database' => '{{database}}',
);
STRING
		);

		$app['path'] = __DIR__;
		$expectedConfigFile = $app['path'].'/config/database.php';

		$expectedString = <<<STRING
<?php
return array(
	'database' => 'bar',
);
STRING;

		// This should trigger an Exception
		$filesystem->shouldReceive('put')->with($expectedConfigFile, $expectedString)->andReturn(false);

		$installer->createDatabaseConfig();
	}

	public function testCreatingDatabaseConfig()
	{
		$installer = new Installer(
			$app        = new Container,
			$platform   = m::mock('Platform\Foundation\Platform'),
			$repository = m::mock('Platform\Installer\Repository')
		);

		$app->bindShared('db', function($app)
		{
			return m::mock('Illuminate\Database\DatabaseManager');
		});

		$app['db']->shouldReceive('reconnect')->once();

		$configFileRegex = '/stubs\/database\/foo\.php$/';

		$repository->shouldReceive('getDatabaseDriver')->andReturn('foo');
		$repository->shouldReceive('getDatabaseConfig')->andReturn(array('database' => 'bar'));

		$app['files'] = $filesystem = m::mock('Illuminate\Filesystem');
		$filesystem->shouldReceive('exists')->with($configFileRegex)->once()->andReturn(true);

		$filesystem->shouldReceive('get')->with($configFileRegex)->andReturn(<<<STRING
<?php
return array(
	'database' => '{{database}}',
);
STRING
		);

		$app['path'] = __DIR__;
		$expectedConfigFile = $app['path'].'/config/database.php';

		$expectedString = <<<STRING
<?php
return array(
	'database' => 'bar',
);
STRING;

		$filesystem->shouldReceive('put')->with($expectedConfigFile, $expectedString)->andReturn(true);

		// We should be unsetting the config
		$app['config'] = $config = m::mock('Illuminate\Config\Repository[set]');
		$config->shouldReceive('set')->with('database', null)->once();

		$installer->createDatabaseConfig();
	}

}
