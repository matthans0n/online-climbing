<footer id="footer">

    <div class="container">

        <div class="col-lg-12">

			<span>
				{{{ Config::get('platform.site.copyright') }}}
			</span>

            <span class="pull-right"></span>

        </div>

    </div>

</footer>
