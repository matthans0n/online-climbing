<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'onlineclimbing-wp');

/** MySQL database username */
define('DB_USER', 'homestead');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_UZYY#[2xiir5o?fs>3E~Qq4SE|VN&}&kNtt]<2f=rW.[*5?/:3K1l>WMyQikb/a');
define('SECURE_AUTH_KEY',  '+GA..ePjUb<Z)bP5i RSyO{7du5n,N+}Emx IpxjX,RWuf~k><HpCtCI~j.H<#PA');
define('LOGGED_IN_KEY',    'o0x;iG{Tc9wrikYvCZJ:FMNAT#Yp]XWyb{:!sr)tT%iwan)yZq01j$-i*Y>APL>d');
define('NONCE_KEY',        'V!T@t-Kdj&z2)h5| :#6pc!J;W4qSZ[t7F^i@&^Z-JKK`Y2O  tX!u !IO*5/dz!');
define('AUTH_SALT',        'YI0EFP01u@DOxL*2vSH?WkeHfz+Mytyv6l+tMGg`c~;)x{>0bwMqyX(?b?rKD#v7');
define('SECURE_AUTH_SALT', 'i^ `tJPfi1qzp/`C4:6Fu %il+RnIYB^}Z?:e{_SB`Q,IZCR}&Db3zl_e@K}=a7Z');
define('LOGGED_IN_SALT',   'efD|=&i%w[O#YVVcD;m(H@9%-uLiXekZ_Y0/=2(zM(9JQP^Kf}T9/s3v`NKe?)%]');
define('NONCE_SALT',       'c@~;9 MtIt]^AlnL{c*%HC=taU}DDd1H@AC~lEl/90s]p&~AKl/`vtr-;on7I{l#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
